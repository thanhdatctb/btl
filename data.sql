﻿use master
go
drop database QLBH;
go
--Tạo Database
Create database QLBH1;
go
use QLBH;
--Tạo bảng HangHoa
Create Table tblLoaiHang
(
	MaL varchar(10) not null,
	TenL nvarchar(15),
	Constraint PK_MaL Primary Key (MaL)
);
go
Create Table tblHangHoa
(
	MaH varchar(10) not null,
	TenH nvarchar(30),
	DVTinh nvarchar(10),
	Giaban int,
	MaL varchar(10),
	LuuKho varchar(20),
	Constraint PK_MaH Primary Key (MaH),
	Foreign Key (MaL) references tblLoaiHang(MaL)
);
go
Select * from tblLoaiHang
--Tạo bảng NhanVien
Create Table tblNhanVien
(
	MaNV varchar(10) not null,
	TenNV nvarchar(30),
	NgaySinh datetime,
	GioiTinh bit,
	DiaChi nvarchar(30),
	SDT varchar(15),
	HSL float,
	Constraint PK_MaNV Primary Key(MaNV)
);
go
Create table tblTaiKhoan
(
MaNV varchar(10)  Foreign Key (MaNV) references tblNhanVien(MaNV) ,
TaiKhoan varchar(50),
MatKhau varchar(50)
)
go
--Tạo bảng NhaCungCap
Create Table tblNhaCungCap
(
	MaNCC varchar(10) not null,
	TenNCC nvarchar(30),
	DiaChi nvarchar(30),
	SDT varchar(30),
	Constraint PK_MaNCC Primary Key(MaNCC)
);
go
--Tạo bảng KhachHang
Create Table tblKhachHang
(
	MaKH varchar(10) not null,
	TenKH nvarchar(30),
	DiaChi nvarchar(30),
	SDT varchar(15),
	Constraint PK_MaKH Primary Key(MaKH)
);
go
--Tạo bảng PhieuNhap
Create Table tblPhieuNhap
(
	SoPN varchar(10) not null,
	NgayNhap datetime,
	DaThanhToan bit,
	MaNV varchar(10),
	MaNCC varchar(10),
	Constraint PK_SoPN Primary Key (SoPN),
	Constraint FK_MaNCC Foreign Key (MaNCC) references tblNhaCungCap(MaNCC),
	Constraint FK_MaNV Foreign Key (MaNV) references tblNhanVien(MaNV)
);
go
--Tạo bảng HoaDon
Create Table tblHoaDon
(
	MaHD varchar(10) not null,
	NgayLap datetime,
	MaNV varchar(10),
	MaKH varchar(10),
	GiamGia float,
	Constraint PK_MaHD Primary Key (MaHD),
	Constraint FK_MaNV_HD Foreign Key (MaNV) references tblNhanVien(MaNV),
	Constraint FK_MaKH Foreign Key (MaKH) references tblKhachHang(MaKH)

);
go
--Tạo bảng ChitietHoaDon
Create Table tblChiTietHD
(
	MaHD varchar(10) not null Foreign Key (MaHD) references tblHoaDon(MaHD),
	MaH varchar(10) not null Foreign Key (MaH) references tblHanghoa(MaH),
	SoLuong int,
	GiaBan int
);
go
--Tạo bảng ChiTietPN
Create Table tblChiTietPN
(
	SoPN varchar(10) not null,
	MaH varchar(10) not null,
	SoLuong int,
	GiaNhap int,
	Constraint FK_SoPN Foreign Key (SoPN) references tblPhieuNhap(SoPN),
	Constraint FK_MaH_2 Foreign Key (MaH) references tblHangHoa(MaH)
);
go
--Tạo Procedure thêm Loại Hàng
Create proc Ins_LoaiHang
@MaL varchar(10),
@TenL nvarchar(15)
As Insert Into tblLoaiHang(MaL,TenL)
Values(@MaL,@TenL);
go
--Tạo Procedure thêm Hàng Hóa
Create proc Ins_HangHoa
@MaH varchar(10),
@TenH nvarchar(30),
@DVTinh nvarchar(10),
@Giaban int,
@MaL varchar(10),
@LuuKho varchar(20)
As Insert Into tblHangHoa(MaH,TenH,DVTinh,Giaban,MaL,LuuKho)
Values(@MaH,@TenH,@DVTinh,@GiaBan,@MaL,@LuuKho);
go
--Tạo Procedure thêm Khách hàng
Create Proc Ins_KhachHang
@MaKH varchar(10),
@TenKH nvarchar(30),
@DiaChi nvarchar(30),
@SDT varchar(15)
As Insert Into tblKhachHang(MaKH,TenKH,DiaChi,SDT)
Values(@MaKH,@TenKH,@DiaChi,@SDT);
go
--Tạo Procedure thêm Nhân Viên
Create proc Ins_NhanVien
@MaNV varchar(10),
@TenNV nvarchar(30),
@NgaySinh datetime,
@GioiTinh bit,
@DiaChi nvarchar(30),
@SDT varchar(15),
@HSL float
As Insert into tblNhanVien(MaNV,TenNV,NgaySinh,GioiTinh,DiaChi,SDT,HSL)
Values (@MaNV,@TenNV,@NgaySinh,@GioiTinh,@DiaChi,@SDT,@HSL);
go
--Tạo Procedure thêm Nhà cung cấp(chua)
Create proc Ins_NhaCungCap
@MaNCC varchar(10),
@TenNCC nvarchar(30),
@DiaChi nvarchar(30),
@SDT varchar(30)
As Insert Into tblNhaCungCap(MaNCC,TenNCC,DiaChi,SDT)
Values (@MaNCC,@TenNCC,@DiaChi,@SDT);
go
--Tạo Procedure thêm Hóa Đơn
create proc Ins_HoaDon
@MaHD varchar(10),
@NgayLap datetime,
@MaNV varchar(10),
@MaKH varchar(10),
@GiamGia float
As Insert Into tblHoaDon(MaHD,NgayLap,MaNV,MaKH,GiamGia)
Values (@MaHD,@Ngaylap,@MaNV,@MaKH,@GiamGia);
go
--Tạo Procedure thêm Chi Tiết Hóa Đơn

create Proc Ins_ChiTietHD
@MaHD varchar(10),
@MaH varchar(10),
@GiaBan int,
@SoLuong int
as
Insert Into tblChiTietHD(MaHD,MaH,GiaBan,SoLuong)
Values (@MaHD,@MaH,@GiaBan,@SoLuong);
--Tạo Procedure thêm Phiếu nhập

go
Create Proc Ins_PhieuNhap
@SoPN varchar(10),
@NgayNhap datetime,
@DaThanhToan bit,
@MaNV varchar(10),
@MaNCC varchar(10)
As Insert Into tblPhieuNhap(SoPN,NgayNhap,DaThanhToan,MaNV,MaNCC)
Values (@SoPN,@NgayNhap,@DaThanhToan,@MaNV,@MaNCC);
go
--Tạo Procedure thêm Chi tiết phiếu nhập
Create Proc Ins_ChiTietPN
@SoPN varchar(10),
@MaH varchar(10),
@SoLuong int,
@GiaNhap int
As Insert Into tblChiTietPN(SoPN,MaH,SoLuong,GiaNhap)
Values (@SoPN,@MaH,@SoLuong,@GiaNhap);
go
--Nhập dữ liệu vào các bảng
--//Nhap du lieu vao bang LoaiHang
Exec Ins_LoaiHang 'L001','Keo';
Exec Ins_LoaiHang 'L002','Banh';
Exec Ins_LoaiHang 'L003','Nuoc Ngot';
Exec Ins_LoaiHang 'L004','Sua';
Exec Ins_LoaiHang 'L005','Xa Phong';
Exec Ins_LoaiHang 'L006','Sua Tam';
Exec Ins_LoaiHang 'L007','Gia Vi';
Exec Ins_LoaiHang 'L008','Ruou';
Exec Ins_LoaiHang 'L009','Nuoc hoa qua';
--//Nhap du lieu vao bang HangHoa

Exec Ins_HangHoa 'H001','Keo Dua','Goi',30000,'L001','Hanoi';
Exec Ins_HangHoa 'H002','Banh Bong Lan','Goi',50000,'L002','Hanoi';
Exec Ins_HangHoa 'H003','Coca Cola','Chai',10000,'L003','Hanoi';
Exec Ins_HangHoa 'H004','Sua Vinamilk','Vi',20000,'L004','Hanoi';
Exec Ins_HangHoa 'H005','Lifebouy','Hop',10000,'L005','Hanoi';
Exec Ins_HangHoa 'H006','Sua Tam Dove','Chai',30000,'L006','Hanoi';
Exec Ins_HangHoa 'H007','Nuoc Mam Nam Ngu','Chai',20000,'L007','Hanoi';
Exec Ins_HangHoa 'H008','Whisky','Chai',1000000,'L008','Hanoi';
Exec Ins_HangHoa 'H009','Keo Deo','Goi',10000,'L001','Hanoi';
Exec Ins_HangHoa 'H010','Keo Bac Ha','Goi',30000,'L001','Hanoi';
Exec Ins_HangHoa 'H011','Banh Quy','Hop',40000,'L002','Hanoi';
Exec Ins_HangHoa 'H012','Banh Ruoc','Goi',15000,'L002','Hanoi';
Exec Ins_HangHoa 'H013','Fanta','Chai',10000,'L003','Hanoi';
Exec Ins_HangHoa 'H014','Sprite','Chai',10000,'L003','Hanoi';
Exec Ins_HangHoa 'H015','Sua Bot','Hop',20000,'L004','Hanoi';
Exec Ins_HangHoa 'H016','Sua Tam DABO Lavender','Chai',130000,'L006','Hanoi';
Exec Ins_HangHoa 'H017','Sua Tam Faceshop White Secret','Chai',200000,'L006','Hanoi';
Exec Ins_HangHoa 'H018','Bot Ngot Ajinomoto','Goi',15000,'L007','Hanoi';
Exec Ins_HangHoa 'H019','Bot Nem Knorr','Goi',17000,'L007','Hanoi';
Exec Ins_HangHoa 'H020','Ruou Nep Cam','Chai',1000000,'L008','Hanoi';
Exec Ins_HangHoa 'H021','Vodka Mamont','Chai',990000,'L008','Hanoi';

--//Nhap du lieu vao bang KhachHang
Exec Ins_KhachHang 'KH001','Nguyen Ha Tran','Ha Noi','0983521464';
Exec Ins_KhachHang 'KH002','Tran Minh Thu','Ha Noi','01692354352';
Exec Ins_KhachHang 'KH003','Nguyen Hoang Lam','Ha Noi','0914624248';
Exec Ins_KhachHang 'KH004','Le Tuyet Nga','Ha Noi','01239855190';
Exec Ins_KhachHang 'KH005','Hoang Minh Anh','Ha Noi','0975527890';
Exec Ins_KhachHang 'KH006','Hoang Thuy Tram','Ha Noi','0975227890';
Exec Ins_KhachHang 'KH007','Vu Kim Ngan','Ha Noi','0975999890';
Exec Ins_KhachHang 'KH008','Nguyen Van Giang','Ha Noi','0635427890';
Exec Ins_KhachHang 'KH009','Nguyen Lan Vy','Ha Noi','0947363890';
Exec Ins_KhachHang 'KH010','Le Tien Anh','Ha Noi','0975735247';

--//Nhap du lieu vao bang NhanVien
Exec Ins_NhanVien 'NV001','Nguyen Thi Thu','1990-03-02',0,'Ha Noi','0982357413',1.1;
Exec Ins_NhanVien 'NV002','Nguyen Hoang An','1988-03-02',1,'Ha Giang','0989823573',1.3;
Exec Ins_NhanVien 'NV003','Hoang Lam Nhi','1990-05-23',0,'Hai Phong','0983854730',1.2;
Exec Ins_NhanVien 'NV004','Tran Van Minh','1990-05-14',1,'Lang Son','0973928473',1.4;
Exec Ins_NhanVien 'NV005','Le Minh Hoang','1989-01-02',1,'Thai Binh','01663849256',1.5;
---------------------------------------------------------------------------------------
--//Nhap du lieu vao bang NhaCungCap
Exec Ins_NhaCungCap 'NCC001','Hai Ha','2 Doi Can','0983471036';
Exec Ins_NhaCungCap 'NCC002','Thanh La','4 Dien Bien Phu','0973571058';
Exec Ins_NhaCungCap 'NCC003','Hoa Nam','23 Hoang Hoa Tham','01662847295';
Exec Ins_NhaCungCap 'NCC004','Chi Mai','50 Giai Phong','0917402748';
Exec Ins_NhaCungCap 'NCC005','Hoa Binh','10 Thanh Nien','0977392847';

--//Nhap du lieu vao bang PhieuNhap
Exec Ins_PhieuNhap 'PN001','2019-02-02',1,'NV001','NCC001';
Exec Ins_PhieuNhap 'PN002','2019-05-04',1,'NV002','NCC005';
Exec Ins_PhieuNhap 'PN003','2019-04-13',0,'NV003','NCC002';
Exec Ins_PhieuNhap 'PN004','2019-10-10',1,'NV004','NCC003';
Exec Ins_PhieuNhap 'PN005','2019-11-11',0,'NV005','NCC004';
Exec Ins_PhieuNhap 'PN006','2019-05-02',1,'NV001','NCC002';
Exec Ins_PhieuNhap 'PN007','2019-02-11',1,'NV001','NCC001';
Exec Ins_PhieuNhap 'PN008','2019-12-02',1,'NV002','NCC003';
Exec Ins_PhieuNhap 'PN009','2019-04-15',1,'NV003','NCC004';
Exec Ins_PhieuNhap 'PN010','2019-05-05',1,'NV004','NCC005';
Exec Ins_PhieuNhap 'PN011','2019-06-05',1,'NV004','NCC005';
Exec Ins_PhieuNhap 'PN012','2019-06-04',1,'NV004','NCC005';

Exec Ins_PhieuNhap 'PN013','2015-06-04',1,'NV004','NCC005';


--//Nhap du lieu vao bang ChiTietPhieuNhap
Exec Ins_ChiTietPN 'PN001','H001',100,20000;
Exec Ins_ChiTietPN 'PN001','H002',150,40000;
Exec Ins_ChiTietPN 'PN002','H003',100,8000;
Exec Ins_ChiTietPN 'PN002','H004',130,18000;
Exec Ins_ChiTietPN 'PN003','H005',130,9000;
Exec Ins_ChiTietPN 'PN004','H006',120,29000;
Exec Ins_ChiTietPN 'PN005','H007',120,18000;
Exec Ins_ChiTietPN 'PN003','H004',130,18000;
Exec Ins_ChiTietPN 'PN003','H010',200,10000;
Exec Ins_ChiTietPN 'PN004','H011',100,40000;
Exec Ins_ChiTietPN 'PN005','H015',100,20000;
Exec Ins_ChiTietPN 'PN006','H011',140,38000;
Exec Ins_ChiTietPN 'PN007','H013',220,9000;
Exec Ins_ChiTietPN 'PN008','H012',300,13000;
Exec Ins_ChiTietPN 'PN009','H014',100,9000;
Exec Ins_ChiTietPN 'PN010','H016',100,129000;
Exec Ins_ChiTietPN 'PN011','H017',100,199000;
Exec Ins_ChiTietPN 'PN012','H017',10,199000;
Exec Ins_ChiTietPN 'PN013','H018',10,14000;
-----------------------------------------------------------------------------------------
--//Nhap du lieu vao bang HoaDon
Exec Ins_HoaDon 'HD001','2019-03-12','NV001','KH002',0;
Exec Ins_HoaDon 'HD002','2019-03-02','NV002','KH003',0;
Exec Ins_HoaDon 'HD003','2019-03-04','NV003','KH001',0;
Exec Ins_HoaDon 'HD004','2019-02-12','NV004','KH005',0;
Exec Ins_HoaDon 'HD005','2019-02-12','NV005','KH004',0;
Exec Ins_HoaDon 'HD006','2019-04-12','NV001','KH008',0;
Exec Ins_HoaDon 'HD007','2019-03-12','NV004','KH009',0;
Exec Ins_HoaDon 'HD008','2019-05-11','NV003','KH007',0;
Exec Ins_HoaDon 'HD009','2019-03-10','NV005','KH006',0;
Exec Ins_HoaDon 'HD010','2019-08-08','NV004','KH010',0;
Exec Ins_HoaDon 'HD011','2019-08-07','NV004','KH010',0;
Exec Ins_HoaDon 'HD012','2019-08-07','NV004','KH010',0;


--//Nhap du lieu vao bang ChiTietHoaDon
Exec Ins_ChiTietHD 'HD001','H001',30000,2;
Exec Ins_ChiTietHD 'HD002','H002',500000,2;
Exec Ins_ChiTietHD 'HD003','H003',150000,3;
Exec Ins_ChiTietHD 'HD003','H004',25000,5;
Exec Ins_ChiTietHD 'HD004','H005',120000,5;
Exec Ins_ChiTietHD 'HD005','H006',100000,3;
Exec Ins_ChiTietHD 'HD006','H010',159000,8;
Exec Ins_ChiTietHD 'HD007','H016',9000,1;
Exec Ins_ChiTietHD 'HD008','H015',15000,2;
Exec Ins_ChiTietHD 'HD009','H018',490000,1;
Exec Ins_ChiTietHD 'HD010','H020',10100,1;
Exec Ins_ChiTietHD 'HD011','H019',100000,1;
Exec Ins_ChiTietHD 'HD012','H018',470000,1;
Exec Ins_ChiTietHD 'HD012','H011',78000,98;
Exec Ins_ChiTietHD 'HD012','H014',6900,98;

--View Danh sách khách hàng
/*Create View DSKhachHang
As Select * From tblKhachHang;
Select * from DSKhachHang

--View Top 5 Sản phẩm bán chạy nhất
Create View Top5_HangHoa
As Select Top 5 tblHangHoa.MaH,TenH,SUM(SoLuong) As N'Số lượng bán được'
from tblHangHoa,tblChiTietHD
where tblHangHoa.MaH=tblChiTietHD.MaH
group by tblHangHoa.MaH,TenH
order by SUM(SoLuong) desc;
Select * from Top5_hangHoa;

--View số lần mua hàng của từng khách hàng
Create View SoLan_MuaHang
As Select tblKhachHang.MaKH,TenKH,COUNT(tblHoaDon.MaHD) As N'Số lần mua hàng'
from tblKhachHang,tblHoaDon
where tblKhachHang.MaKH=tblHoaDon.MaKH
group by tblKhachHang.MaKH,TenKH;
Select * from SoLan_MuaHang
--Tạo Procedure sửa giá hàng hóa
Create Proc SuaGia_HangHoa
@MaH varchar(10),
@GiaBan int
As Update tblHangHoa
Set Giaban=@GiaBan
where MaH=@MaH;

--Procedure Danh sách Hóa Đơn của 1 khách hàng
Create Proc HoaDon_KhachHang
@MaKH varchar(10)
As
Select TenKH,MaHD,NgayLap
from tblKhachHang,tblHoaDon
where tblKhachHang.MaKH=tblHoaDon.MaKH and tblHoaDon.MaKH=@MaKH;

--Procedure Chi tiết của 1 Hóa đơn
Create Proc ChiTiet_HoaDon
@MaHD varchar(10)
As
Select tblHoaDon.MaHD,MaH,SoLuong,GiaBan 
from tblHoaDon,tblChiTietHD
where tblHoaDon.MaHD=tblChiTietHD.MaHD and tblHoaDon.MaHD=@MaHD;

--Procedure Danh sách các phiếu nhập hàng chưa thanh toán trong tháng
Create Proc ChuaThanhToan_PN
@y int,
@m int
As
Select SoPN,NgayNhap,MaNCC from tblPhieuNhap
where YEAR(NgayNhap)=@y and MONTH(NgayNhap)=@m and DaThanhToan=0;
Exec ChuaThanhToan_PN 2015,4
--Procedure số hóa đơn đã lập của từng nhân viên trong tháng
Create Proc SLHoaDon_NhanVien
@y int,
@m int
As Select tblNhanVien.MaNV,TenNV,COUNT(tblHoaDon.MaHD) As N'Số lượng hóa đơn'
from tblNhanVien,tblHoaDon
where tblNhanVien.MaNV=tblHoaDon.MaNV and YEAR(NgayLap)=@y and MONTH(NgayLap)=@m
group by tblNhanVien.MaNV,TenNV;
Exec SLHOADON_NHANVIEN 2015,3

--Tao Trigger dua ra thong bao moi khi co du lieu duoc them vao phieu nhap
create trigger ThongBaoThem
on tblPhieuNhap
for insert
as
raiserror(N'Du lieu vua duoc them moi', 16,1)

--------------------------------------------------
--------------------------------------------------
--Tao user Phuong/12a1
exec sp_addrole 'ThemLoaiHang','dbo'
grant exec on Ins_LoaiHang to ThemLoaiHang
exec sp_addlogin 'Phuong12a1','12a1'
exec sp_grantdbaccess 'Phuong12a1'
exec sp_addrolemember 'ThemLoaiHang','Phuong12a1'
revoke on tblHangHoa

----
create trigger KTraHangTon
on tblChiTietHD
after insert
as 
begin
	declare @ton int,
	@slnhap int,
	@slban int
	select @slnhap = SoLuong from tblChiTietPN
	select @slban = SoLuong from tblChiTietHD
	if @slnhap-@slban<10
	raiserror (N'Hang ton khong qua 10 don vi',16,1)
	rollback transaction
end


create trigger HangHoaTon
on tblChiTietHD
for update 
as 
begin
	declare @ton int,
	@slnhap int,
	@slban int
	select @slnhap = SoLuong from tblChiTietPN
	select @slban = SoLuong from tblChiTietHD
	if @slnhap-@slban>10
	raiserror (N'Hang ton lon hon 10 don vi',16,1)
	rollback transaction
end

------
create trigger KiemTraHangTon
on tblChiTietHD
for update 
as 
begin
	declare @ton int,
	@slnhap int,
	@slban int
	select @slnhap = SoLuong from tblChiTietPN
	select @slban = SoLuong from tblChiTietHD
	if @slnhap-@slban<10
	raiserror (N'Hang ton khong qua 10 don vi',16,1)
	rollback transaction
end

*/

-------------------------TẠO VEIW------------------
----------------------------------------------------

---------------------------------------------------
---1. Tạo view xem tblLoaiHang(chua >>>>)
IF OBJECT_ID('vLoaiHang') is not null
	DROP VIEW vLoaiHang
GO
CREATE VIEW vLoaiHang
AS
SELECT MaL AS [Mã Loại Hàng],
	TenL AS [Tên Loại Hàng]
FROM tblLoaiHang
GO

SELECT * FROM vLoaiHang

--------------------------------------------------
---2. Tạo view xem tblKhachhang
IF OBJECT_ID('vKhachhang') is not null
	DROP VIEW vKhachhang
GO
CREATE VIEW vKhachhang
AS
SELECT MaKH AS [Mã Khách Hàng],
	TenKH AS [Tên Khách Hàng],
	DiaChi AS [Địa chỉ],
	SDT AS [Số điện thoại]
FROM tblKhachHang
GO

SELECT * FROM vKhachhang

-----------------------------------------------
---3. Tạo view xem tblNhanvien
IF OBJECT_ID('vNhanvien') is not null
	DROP VIEW vNhanvien
GO
CREATE VIEW vNhanvien
AS
SELECT MaNV AS [Mã nhân viên],
	TenNV AS [Tên nhân viên],
	NgaySinh AS [Ngày sinh],
	GioiTinh AS [Giới tính],
	DiaChi AS [Địa chỉ],
	SDT AS [Số điện thoại],
	HSL AS [Hề số lương]
FROM tblNhanVien
GO

SELECT * FROM vNhanvien

-------------------------------------------------------
---4. Tạo view xem tblHanghoa
IF OBJECT_ID('vHanghoa') is not null
	DROP VIEW vHanghoa
GO
CREATE VIEW vHanghoa
AS
SELECT MaH AS [Mã hàng hóa],
	TenH AS [Tên hàng hóa],
	DVTinh AS [Đơn vị tính],
	Giaban AS [Giá bán],
	tblLoaiHang.MaL AS [Mã loại hàng]
FROM tblHangHoa INNER JOIN tblLoaiHang ON tblHangHoa.MaL = tblLoaiHang.MaL
GO

SELECT * FROM vHanghoa



--------------------------------------------------------
--5. Tạo View xem tblHoadon(chua chay) >>>
IF OBJECT_ID('vHoadon') is not null
	DROP VIEW vHoadon
GO
create VIEW vHoadon
AS
SELECT tblHoaDon.MaHD AS [Mã hóa đơn],
	NgayLap AS [Ngày lập],
	tblNhanVien.TenNV AS [Tên nhân viên],
	tblKhachHang.TenKH AS [Tên khách hàng],
	GiamGia  as [Giảm giá]

FROM (tblHoaDon INNER JOIN tblNhanVien ON tblHoaDon.MaNV = tblNhanVien.MaNV)
	INNER JOIN tblKhachHang ON tblHoaDon.MaKH = tblKhachHang.MaKH
GO

SELECT * FROM vHoadon where [Mã hóa đơn] = 'HD11'
-----------------------------------------------------------
---6. Tạo View xem tblPhieunhap
IF OBJECT_ID('vPhieunhap') is not null
	DROP VIEW vPhieunhap
GO
CREATE VIEW vPhieunhap
AS
SELECT  tblPhieuNhap.SoPN AS [Số phiếu nhập],
	NgayNhap AS [Ngày nhập],
	DaThanhToan AS [Đã thanh toán],
	tblNhanVien.MaNV AS [Mã nhân viên],
	tblNhaCungCap.MaNCC AS [Mã Nhà Cung Cấp]
FROM (tblPhieuNhap INNER JOIN tblNhanVien ON tblPhieuNhap.MaNV = tblNhanVien.MaNV)
	INNER JOIN tblNhaCungCap ON tblPhieuNhap.MaNCC = tblNhaCungCap.MaNCC
GO
---------------------------------------------------------------
---7. Tạo View xem tblNhacungcap
IF OBJECT_ID('vNhacungcap') is not null
	DROP VIEW vNhacungcap
GO
CREATE VIEW vNhacungcap
AS
SELECT MaNCC AS [Mã Nhà Cung Cấp],
	TenNCC AS [Tên Nhà Cung Cấp],
	DiaChi AS [Địa Chỉ],
	SDT AS [Số điện thoại]
FROM tblNhaCungCap
GO

SELECT * FROM vNhacungcap
---------------------------------------------------------------
--8. tạo view xem tblChiTietHD
IF OBJECT_ID('vChitietHD') is not null
	DROP VIEW vChitietHD
GO
create VIEW vChitietHD
AS
SELECT tblChiTietHD.MaHD AS [Mã hóa đơn],
	tblHangHoa.TenH AS [Tên hàng hóa],
	tblChiTietHD.SoLuong AS [Số lượng],
	tblChiTietHD.GiaBan AS [Giá bán],
	tblChiTietHD.SoLuong*tblChiTietHD.GiaBan AS [Thành tiền]
FROM (tblChiTietHD INNER JOIN tblHangHoa ON tblChiTietHD.MaH = tblHangHoa.MaH)
	INNER JOIN tblHoaDon ON tblHoaDon.MaHD = tblChiTietHD.MaHD
GO
select * from vChitietHD
if OBJECT_ID('vChitietPN') is not null
drop view vChitietPN 
go
create view vChitietPN
as
select tblChitietPN
----------------Tạo proc sửa-------------
--------------------------------------
--1. proc sửa tblLoaihang
create proc spSuaLH
(	@MaL nvarchar(5),
	@TenL nVarchar(30),
	@Macansua nvarchar(5)
)
AS
Begin
	Update tblLoaiHang 
	Set MaL=@MaL,
		TenL=@TenL
	Where MaL=@Macansua
End
Go
--2. tạo proc sửa tblHangHoa
create Proc spSuaHH
	@MaH nvarchar(5),
	@TenH nVarchar(30),
	@DVT nvarchar(10),
	@Giaban int,
	@MaL nvarchar(10),
	@Macansua nvarchar(5)

AS
Begin
	Update tblHangHoa 
	Set 
		TenH=@TenH,
		DVTinh=@DVT,
		Giaban=@Giaban,
		MaL=@MaL
	Where MaH=@Macansua
End
Go
--3. tao proc sửa tblKhachang
Create Proc spSuaKH
(	@MaKH nvarchar(5),
	@TenKH nVarchar(30),
	@Diachi nvarchar(30),
	@SDT varchar,
	@Macansua nvarchar(5)
)
AS
Begin
	Update tblKhachHang
	Set MaKH=@MaKH,
		TenKH=@TenKH,
		DiaChi=@Diachi,
		SDT=@SDT
	Where MaKH=@Macansua
End
Go
--4 tạo proc sửa tblNhacungcap
Create Proc spSuaNCC
(	@MaNCC nvarchar(10),
	@TenNCC nVarchar(30),
	@Diachi nvarchar(30),
	@SDT varchar,
	@Macansua nvarchar(10)
)
AS
Begin
	Update tblNhaCungCap
	Set MaNCC=@MaNCC,
		TenNCC=@TenNCC,
		DiaChi=@Diachi,
		SDT=@SDT
	Where MaNCC=@Macansua
End
Go
--5 tạo proc sửa tblHoadon
create Proc spSuaHD
	@MaHD nvarchar(10),
	@Ngaylap datetime,
	@MaNV varchar(10),
	@MaKH varchar(10),
	@Giamgia float
	--@Macansua nvarchar(10)

AS
Begin
	Update tblHoaDon
	Set 
		NgayLap=@Ngaylap,
		MaNV=@MaNV,
		MaKH=@MaKH,
		GiamGia=@Giamgia
	Where MaHD=@MaHD
End
Go
select * from vHoadon
 exec  spSuaHD 'HD123' , '2002-02-02 00:00:00.000', 'Hoang Lam Nhi','Hoang Lam Nhi',1
--6 tạo proc sửa tblPhieunhap
Create Proc spSuaPN
(	@SoPN varchar(10),
	@Ngaynhap datetime,
	@Dathanhtoan bit,
	@MaNV varchar(10),
	@MaNCC varchar(10),
	@Macansua nvarchar(5)
)
AS
Begin
	Update tblPhieuNhap
	Set SoPN=@SoPN,
		NgayNhap=@Ngaynhap,
		MaNV=@MaNV,
		MaNCC=@MaNCC
	Where SoPN=@Macansua
End
Go
--7 tạo proc sửa tblNhanvien
create Proc spSuaNV
(	@MaNV varchar(10),
	@TenNV nVarchar(30),
	@Ngaysinh datetime,
	@Gioitinh bit,
	@Diachi nvarchar(30),
	@SDT varchar(15),
	@HSL float,
	@Macansua nvarchar(10)
)
AS
Begin
	Update tblNhanVien
	Set MaNV=@MaNV,
		TenNV=@TenNV,
		NgaySinh=@Ngaysinh,
		GioiTinh=@Gioitinh,
		DiaChi=@Diachi,
		SDT=@SDT
	Where MaNV=@Macansua
End
Go
----------------Tạo proc xóa-------------
--------------------------------------------
---1. tạo proc xóa bản ghi cho tblLoaihang
Create Proc spXoaLH
(	@MaCanxoa nvarchar(5)
)
AS
Begin
	Delete from tblLoaiHang 
	Where MaL=@MaCanxoa
End
Go
--2. tạo proc xóa bản ghi cho tblHangHoa
Create Proc spXoaHH
(	@MaCanxoa nvarchar(5)
)
AS
Begin
	Delete from tblHangHoa 
	Where MaH=@MaCanxoa
End
Go
--3 tạo proc xóa cho tblNhanvien
Create Proc spXoaNV
(	@MaCanxoa nvarchar(10)
)
AS
Begin
	Delete from tblNhanVien 
	Where MaNV=@MaCanxoa
End
Go
exec spXoaNV 'NV0051'
--4 tạo proc xóa cho tblKhachhang
Create Proc spXoaKH
(	@MaCanxoa nvarchar(5)
)
AS
Begin
	Delete from tblKhachHang 
	Where MaKH=@MaCanxoa
End
Go
--5 tạo proc xóa cho tblNhacungcap
Create Proc spXoaNCC
(	@MaCanxoa nvarchar(10)
)
AS
Begin
	Delete from tblNhaCungCap 
	Where MaNCC=@MaCanxoa
End
Go
--6 tạo proc xóa cho tblHoadon
Create Proc spXoaHD
(	@MaCanxoa nvarchar(5)
)
AS
Begin
	Delete from tblHoaDon
	Where MaHD=@MaCanxoa
End
Go
--7 tạo proc xóa cho tblPhieunhap
Create Proc spXoaPN
(	@MaCanxoa nvarchar(5)
)
AS
Begin
	Delete from tblPhieuNhap 
	Where SoPN=@MaCanxoa
End
Go
---------------------------------------
--tạo view xem danh sách nhân viên
create proc spTongtien
@MaHD varchar(10)
AS
select [Mã hóa đơn],Sum([Thành tiền]) as [Tổng tiền] from vChitietHD
group by [Mã hóa đơn]

-----------------------------------
1
select * from tblNhanVien , tblTaiKhoan where tblNhanVien.MaNV = tblTaiKhoan.MaNV
2
alter proc spSuaHD 
@GiamDoc varchar(10)
as

