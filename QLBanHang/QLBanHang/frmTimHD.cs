﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QLBanHang
{
    public partial class FrmTimHD : Form
    {
        GlobalFuncion gf = new GlobalFuncion();
        string sView = "vHoadon";
        string sTimkiem = "";
        public FrmTimHD()
        {
            InitializeComponent();
        }

        private void FrmTimHD_Load(object sender, EventArgs e)
        {
            gf.fHienthiDulieuDatagridView(sView, dgrTimHD);
            txtDieukien.Visible = false;
            sTimkiem = sView;
        }

        private void rbtTatca_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = false;
        }

        private void rbtTimtheoMaHD_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = true;
            txtDieukien.Top = rbtTimtheoMaHD.Top;
        }

        private void rbtTimtheongaylap_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = true;
            txtDieukien.Top = rbtTimtheongaylap.Top;
        }

        private void rbtTimtheoTenKH_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = true;
            txtDieukien.Top = rbtTimtheoTenKH.Top;
        }

        private void btnTim_Click(object sender, EventArgs e)
        {
            string strSQL = sView;
            if (rbtTimtheoMaHD.Checked)
                strSQL = strSQL + " WHERE [Mã Hóa Đơn]='" + txtDieukien.Text + "'";
            if (rbtTimtheongaylap.Checked)
                strSQL = strSQL + " WHERE [Ngày lập] " + txtDieukien.Text + "'";
            if (rbtTimtheoTenKH.Checked)
                strSQL = strSQL + " WHERE [Tên khách hàng] like N'%" + txtDieukien.Text + "'";
            gf.fHienthiDulieuDatagridView(strSQL, dgrTimHD);
            sTimkiem = strSQL;
        }

        private void btnIn_Click(object sender, EventArgs e)
        {

        }
    }
}
