﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace QLBanHang
{
    public partial class frmHoadon : Form
    {
        GlobalFuncion gf = new GlobalFuncion();
        string sView = "";
        public frmHoadon()
        {
            InitializeComponent();
        }

        private void frmHoadon_Load(object sender, EventArgs e)
        {
          //  gf.fHienthiDulieuComboBox("tblNhanVien", "TenNV", "MaNV", cboNV);
            gf.fHienthiDulieuComboBox("tblKhachHang", "TenKh", "MaKH", cboKH);
            gf.fHienthiDulieuComboBox("tblHangHoa", "TenH", "MaH", cboTenHH);
            txtNhanvien.Text = gf.XemDL("select TenNV from tblNhanVien where MaNV = '" + frmDangnhap.TenND + "'").Rows[0][0].ToString().Trim();
            txtTongtien.ReadOnly = false;
            btnSua.Enabled = false;
            btnThemSP.Enabled = false;
            btnXemHD.Enabled = false;
            //txtTenNV.ReadOnly = true;
            
        }
        public static string maHoaDon;
        private void btnThem_Click(object sender, EventArgs e)
        {
            if (gf.fKiemtraXaurong(txtMaHD.Text) == true || gf.fKiemtraXaurong(txtNgaylap.Text) == true || gf.fKiemtraXaurong(txtGiamgia.Text) == true)
            {
                MessageBox.Show("Chưa nhập đủ thông tin hóa đơn", "Chú ý");
                return;
            }
            if (gf.fKiemtraTrungkhoa("tblHoaDon", "MaHD", txtMaHD.Text) == true)
            {
                MessageBox.Show("Mã hóa đơn này đã có!", "Chú ý");
                return;
            }
            // check ngay hoa don va thoi gian
            TimeSpan ngay;
            ngay = (DateTime.Now - DateTime.Parse(txtNgaylap.Text));
            if (DateTime.Parse(txtNgaylap.Text) > DateTime.Now || int.Parse(ngay.Days.ToString()) > 7)
            {
                MessageBox.Show("Ngày lập hóa đơn phải nhỏ hơn ngày hiện tại( không quá 7 ngày!)", "Chú ý");
                return;
            }
            //string MaKH, MaNV;
            string MaKH;
            maHoaDon = txtMaHD.Text;
            MaKH = cboKH.SelectedValue.ToString();
        //  MaNV = cboNV.SelectedValue.ToString();
            string MaNV = gf.XemDL("select MaNV from tblNhanVien where MaNV = '" + frmDangnhap.TenND + "'").Rows[0][0].ToString().Trim();
            try
            {
                if (gf.fKetNoiCSDL() == false)
                    return;
                SqlCommand cmd = new SqlCommand("Ins_HoaDon", gf.myCnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MaHD", txtMaHD.Text);
                cmd.Parameters.AddWithValue("@MaNV", MaNV);
                cmd.Parameters.AddWithValue("@NgayLap", DateTime.Parse(txtNgaylap.Text));
                cmd.Parameters.AddWithValue("@MaKH",MaKH);
                cmd.Parameters.AddWithValue("@GiamGia", txtGiamgia.Text);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                gf.fHienthiDulieuDatagridView("vHoadon where [Mã hóa đơn] ='" + txtMaHD.Text + "'", dgrHoadon);
                btnSua.Enabled = true;
                btnThemSP.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Có lỗi khi thêm hóa đơn!", "Chú ý");
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (gf.fKiemtraXaurong(txtMaHD.Text) == true || gf.fKiemtraXaurong(txtNgaylap.Text) == true || gf.fKiemtraXaurong(txtGiamgia.Text) == true)
            {
                MessageBox.Show("Chưa nhập đủ thông tin hóa đơn", "Chú ý");
                return;
            }
            // check ngay hoa don va thoi gian
            TimeSpan ngay;
            ngay = (DateTime.Now - DateTime.Parse(txtNgaylap.Text));
            if (DateTime.Parse(txtNgaylap.Text) > DateTime.Now || int.Parse(ngay.Days.ToString()) > 7)
            {
                MessageBox.Show("Ngày lập hóa đơn phải nhỏ hơn ngày hiện tại và không được quá 7 ngày!", "Chú ý");
                return;
            }
            string MaKH, MaNV;
            MaKH = cboKH.SelectedValue.ToString();
            //  MaNV = cboNV.SelectedValue.ToString();
          //  MaNV = cboNV.SelectedValue.ToString();
             MaNV = gf.XemDL("select MaNV from tblNhanVien where MaNV = '" + frmDangnhap.TenND + "'").Rows[0][0].ToString().Trim();
            int nVitri = dgrHoadon.CurrentRow.Index;
                if (gf.fKetNoiCSDL() == false)
                    return;
                SqlCommand cmd = new SqlCommand("spSuaHD", gf.myCnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MaHD", txtMaHD.Text);
                cmd.Parameters.AddWithValue("@MaNV", MaNV);
                cmd.Parameters.AddWithValue("@NgayLap", DateTime.Parse(txtNgaylap.Text));
                cmd.Parameters.AddWithValue("@MaKH", MaKH);
                cmd.Parameters.AddWithValue("@GiamGia", txtGiamgia.Text);
               

            //   cmd.Parameters.AddWithValue("@Macansua", dgrHoadon.CurrentRow.Cells[0].Value.ToString());
            cmd.ExecuteNonQuery();
                cmd.Dispose();
                gf.fHienthiDulieuDatagridView("vHoadon where [Mã hóa đơn] ='" + txtMaHD.Text + "'", dgrHoadon);
                //Chọn dòng vừa sửa
                dgrHoadon.ClearSelection();
                dgrHoadon.CurrentCell = dgrHoadon.Rows[nVitri].Cells[0];
                dgrHoadon.Rows[nVitri].Selected = true;
        }

        private void btnThemSP_Click(object sender, EventArgs e)
        {
            if (gf.fKiemtraXaurong(txtSL.Text) == true || gf.fKiemtraXaurong(txtGB.Text) == true)
            {
                MessageBox.Show("Chưa nhập đủ thông tin hàng hóa", "Chú ý");
                return;
            }
            string MaH;
            MaH = cboTenHH.SelectedValue.ToString();
           // try
            //{
                if (gf.fKetNoiCSDL() == false)
                    return;
                SqlCommand cmd = new SqlCommand("Ins_ChiTietHD", gf.myCnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MaHD", txtMaHD.Text);
                cmd.Parameters.AddWithValue("@MaH", MaH);
                cmd.Parameters.AddWithValue("@GiaBan",txtGB.Text);
                cmd.Parameters.AddWithValue("@SoLuong", txtSL.Text);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                string tongtien = gf.XemDL("select Sum([Thành tiền]) as [Tổng tiền] from vChitietHD where [Mã hóa đơn] = '" + txtMaHD.Text + "' group by [Mã hóa đơn]").Rows[0][0].ToString().Trim();
                txtTongtien.Text = tongtien;
                gf.fHienthiDulieuDatagridView("vChitietHD where [Mã hóa đơn] ='" + txtMaHD.Text + "'", dgrChitietHD);
         
                btnXemHD.Enabled = true;
            //}
            //catch
            //{
              //  MessageBox.Show("Có lỗi khi thêm sản phẩm!", "Chú ý");
          //  }
        }

        private void btnXemHD_Click(object sender, EventArgs e)
        {
            frmXemCTHD f = new frmXemCTHD(sView);
            f.Show();

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
