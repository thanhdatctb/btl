﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace QLBanHang
{
    public partial class frmNhanvien : Form
    {
        GlobalFuncion gf = new GlobalFuncion();
        string sView = "vNhanvien";
        public frmNhanvien()
        {
            InitializeComponent();
        }

        private void frmNhanvien_Load(object sender, EventArgs e)
        {
            gf.fHienthiDulieuDatagridView(sView, dgrNhanvien);
        }

        private void dgrNhanvien_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (dgrNhanvien.Rows.Count <= 0 || dgrNhanvien.CurrentRow.Index == dgrNhanvien.Rows.Count - 1)
                return;
            txtMaNV.Text = "" + dgrNhanvien.CurrentRow.Cells[0].Value.ToString();
            txtTenNV.Text = "" + dgrNhanvien.CurrentRow.Cells[1].Value.ToString();
            DateTime d = DateTime.Parse(dgrNhanvien.CurrentRow.Cells[2].Value.ToString());
            txtNS.Text = "" + d.ToShortDateString();
            rbtNam.Checked = dgrNhanvien.CurrentRow.Cells[3].Value.ToString() == "False" ? true : false;
            rbtNu.Checked = !rbtNam.Checked;
            txtDC.Text = "" + dgrNhanvien.CurrentRow.Cells[4].Value.ToString();
            txtSDT.Text = "" + dgrNhanvien.CurrentRow.Cells[5].Value.ToString();
            txtHSL.Text = "" + dgrNhanvien.CurrentRow.Cells[6].Value.ToString();

        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            
          //  if (gf.fKiemtraXaurong(txtMaNV.Text) == true || gf.fKiemtraXaurong(txtDC.Text) == true || gf.fKiemtraXaurong(txtTenNV.Text) == true || gf.fKiemtraXaurong(txtNS.Text) == true || gf.fKiemtraXaurong(txtSDT.Text) == true || gf.fKiemtraXaurong(txtHSL.Text) == true)
         //   {
         //       MessageBox.Show("Chưa nhập đủ thông tin nhân viên", "Chú ý");
          //      return;
           // }
           // if (gf.fKiemtraNgaythang(txtNS.Text) == false)
           // {
           //     MessageBox.Show("Sai ngay tháng!", "Chú ý");
           //     return;
           // }
           // if (gf.fKiemtraTrungkhoa("tblNhanVien", "MaNV", txtMaNV.Text) == true)
         //  {
          //      MessageBox.Show("Mã Nhân Viên này đã có!", "Chú ý");
          //      return;
          //  }
           // if (gf.fKiemtraSothuc(txtHSL.Text) == false)
           // {
             //   MessageBox.Show("Sai hệ số lương!", "Chú ý");
             //   return;
           // }
            
               // {

           // }

           try
           {
                if (gf.fKetNoiCSDL() == false)
                    return;
                SqlCommand cmd = new SqlCommand("Ins_NhanVien", gf.myCnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MaNV", txtMaNV.Text);
                cmd.Parameters.AddWithValue("@TenNV", txtTenNV.Text);
                cmd.Parameters.AddWithValue("@NgaySinh",DateTime.Parse(txtNS.Text));
                cmd.Parameters.AddWithValue("@GioiTinh", rbtNam.Checked == true ? "False" : "True");
               cmd.Parameters.AddWithValue("@DiaChi", txtDC.Text);
                cmd.Parameters.AddWithValue("@SDT", txtSDT.Text);
                cmd.Parameters.AddWithValue("@HSL", float.Parse(txtHSL.Text));
                cmd.ExecuteNonQuery();
               cmd.Dispose();
                gf.fHienthiDulieuDatagridView(sView, dgrNhanvien);
            }
           catch
         {
               MessageBox.Show("Có lỗi khi thêm dữ liệu!", "Chú ý");
          }
       }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (gf.fKiemtraXaurong(txtMaNV.Text) == true || gf.fKiemtraXaurong(txtDC.Text) == true || gf.fKiemtraXaurong(txtTenNV.Text) == true || gf.fKiemtraXaurong(txtNS.Text) == true || gf.fKiemtraXaurong(txtSDT.Text) == true || gf.fKiemtraXaurong(txtHSL.Text) == true)
            {
                MessageBox.Show("Chưa nhập đủ thông tin nhân viên", "Chú ý");
                return;
            }
            if (gf.fKiemtraNgaythang(txtNS.Text) == false)
            {
                MessageBox.Show("Sai ngày tháng!", "Chú ý");
                return;
            }
            if (gf.fKiemtraSothuc(txtHSL.Text) == false)
            {
                MessageBox.Show("Sai hệ số lương!", "Chú ý");
                return;
            }

           try
           {
                int nVitri = dgrNhanvien.CurrentRow.Index;
                if (gf.fKetNoiCSDL() == false)
                    return;
                SqlCommand cmd = new SqlCommand("spSuaNV", gf.myCnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MaNV", txtMaNV.Text);
                cmd.Parameters.AddWithValue("@TenNV", txtTenNV.Text);
                cmd.Parameters.AddWithValue("@NgaySinh", DateTime.Parse(txtNS.Text));
                cmd.Parameters.AddWithValue("@GioiTinh", rbtNam.Checked == true ? "False" : "True");
                cmd.Parameters.AddWithValue("@DiaChi", txtDC.Text);
                cmd.Parameters.AddWithValue("@SDT", txtSDT.Text);
                cmd.Parameters.AddWithValue("@HSL", float.Parse(txtHSL.Text));
                cmd.Parameters.AddWithValue("@Macansua", dgrNhanvien.CurrentRow.Cells[0].Value.ToString());
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                gf.fHienthiDulieuDatagridView(sView, dgrNhanvien);
                //Chọn dòng vừa sửa
                dgrNhanvien.ClearSelection();
                dgrNhanvien.CurrentCell = dgrNhanvien.Rows[nVitri].Cells[0];
                dgrNhanvien.Rows[nVitri].Selected = true;
           }
           catch
          {
              MessageBox.Show("Có lỗi khi sửa dữ liệu!", "Chú ý");
           }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (dgrNhanvien.RowCount <= 0)
                return;
            if (MessageBox.Show("Bạn có muốn xúa nhân viên này?", "Chú ý", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;
            try
            {
                if (!gf.fKetNoiCSDL())
                    return;
                using (SqlCommand cmd = new SqlCommand("spXoaNV", gf.myCnn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@MaCanxoa", dgrNhanvien.CurrentRow.Cells[0].Value.ToString());
                    cmd.ExecuteNonQuery();
                    gf.fHienthiDulieuDatagridView(sView, dgrNhanvien);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi: " + ex.Message, "Thông báo");
            }
        }

        private void dgrNhanvien_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
