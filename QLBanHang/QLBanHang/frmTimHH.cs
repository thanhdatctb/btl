﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QLBanHang
{
    public partial class frmTimHH : Form
    {
        GlobalFuncion gf = new GlobalFuncion();
        string sView = "vHangHoa";
        string sTimkiem = "";
        public frmTimHH()
        {
            InitializeComponent();
        }

        private void frmTimHH_Load(object sender, EventArgs e)
        {
            gf.fHienthiDulieuDatagridView(sView, dgrTimHH);
            txtDieukien.Visible = false;
            cboDVT.Visible = false;
            sTimkiem = sView;
        }

        private void rbtTatca_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = false;
            cboDVT.Visible = false;
        }

        private void rbtTimtheoMaHH_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = true;
            txtDieukien.Top = rbtTimtheoMaHH.Top;
            cboDVT.Visible = false;
        }

        private void rbtTimtheoTen_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = true;
            txtDieukien.Top = rbtTimtheoTen.Top;
            cboDVT.Visible = false;
        }

        private void rbtTimtheoDVT_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = false;
            cboDVT.Visible = true;
            txtDieukien.Top = rbtTimtheoDVT.Top;
        }

        private void btnTim_Click(object sender, EventArgs e)
        {
            string strSQL = sView;
            if (rbtTimtheoMaHH.Checked)
                strSQL = strSQL + " WHERE [Mã hàng hóa]='" + txtDieukien.Text + "'";
            if (rbtTimtheoTen.Checked)
                strSQL = strSQL + " WHERE [Tên hàng hóa] like N'%" + txtDieukien.Text + "'";
            if (rbtTimtheoDVT.Checked)
                strSQL = strSQL + " WHERE [Đơn vị tính] like N'%" + txtDieukien.Text + "'";
            gf.fHienthiDulieuDatagridView(strSQL, dgrTimHH);
            sTimkiem = strSQL;
        }

        private void dgrTimHH_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
