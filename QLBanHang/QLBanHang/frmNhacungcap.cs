﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace QLBanHang
{
    public partial class frmNhacungcap : Form
    {
        GlobalFuncion gf = new GlobalFuncion();
        string sView = "vNhacungcap";
        public frmNhacungcap()
        {
            InitializeComponent();
        }

        private void frmNhacungcap_Load(object sender, EventArgs e)
        {
            gf.fHienthiDulieuDatagridView(sView, dgrNhaCC);
        }

        private void dgrNhaCC_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgrNhaCC.Rows.Count <= 0 || dgrNhaCC.CurrentRow.Index == dgrNhaCC.Rows.Count - 1)
                return;
            txtMaNCC.Text = "" + dgrNhaCC.CurrentRow.Cells[0].Value.ToString();
            txtTenNCC.Text = "" + dgrNhaCC.CurrentRow.Cells[1].Value.ToString();
            txtDiachi.Text = "" + dgrNhaCC.CurrentRow.Cells[2].Value.ToString();
            txtSDT.Text = "" + dgrNhaCC.CurrentRow.Cells[3].Value.ToString();

        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if (gf.fKiemtraXaurong(txtMaNCC.Text) == true || gf.fKiemtraXaurong(txtTenNCC.Text) == true || gf.fKiemtraXaurong(txtDiachi.Text) == true || gf.fKiemtraXaurong(txtSDT.Text) == true)
            {
                MessageBox.Show("Chưa nhập đủ thông tin Nhà cung cấp", "Chú ý");
                return;
            }
            if (gf.fKiemtraTrungkhoa("tblNhaCungCap", "MaNCC", txtMaNCC.Text) == true)
            {
                MessageBox.Show("Mã Nhà cung cấp này đã có!", "Chú ý");
                return;
            }
            try
            {
                if (gf.fKetNoiCSDL() == false)
                    return;
                SqlCommand cmd = new SqlCommand("Ins_NhaCungCap", gf.myCnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MaNCC", txtMaNCC.Text);
                cmd.Parameters.AddWithValue("@TenNCC", txtTenNCC.Text);
                cmd.Parameters.AddWithValue("@DiaChi", txtDiachi.Text);
                cmd.Parameters.AddWithValue("@SDT", txtSDT.Text);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                gf.fHienthiDulieuDatagridView(sView, dgrNhaCC);
            }
            catch
            {
                MessageBox.Show("Có lỗi khi thêm dữ liệu!", "Chú ý");
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (gf.fKiemtraXaurong(txtMaNCC.Text) == true || gf.fKiemtraXaurong(txtTenNCC.Text) == true || gf.fKiemtraXaurong(txtDiachi.Text) == true || gf.fKiemtraXaurong(txtSDT.Text) == true)
            {
                MessageBox.Show("Chưa nhập đủ thông tin Nhà cung cấp", "Chú ý");
                return;
            }
            try
            {
                int nVitri = dgrNhaCC.CurrentRow.Index;
                if (gf.fKetNoiCSDL() == false)
                    return;
                SqlCommand cmd = new SqlCommand("spSuaNCC", gf.myCnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MaNCC", txtMaNCC.Text);
                cmd.Parameters.AddWithValue("@TenNCC", txtTenNCC.Text);
                cmd.Parameters.AddWithValue("@Diachi", txtDiachi.Text);
                cmd.Parameters.AddWithValue("@SDT", txtSDT.Text);
                cmd.Parameters.AddWithValue("@Macansua", dgrNhaCC.CurrentRow.Cells[0].Value.ToString());
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                gf.fHienthiDulieuDatagridView(sView, dgrNhaCC);
                //Chọn dòng vừa sửa
                dgrNhaCC.ClearSelection();
                dgrNhaCC.CurrentCell = dgrNhaCC.Rows[nVitri].Cells[0];
                dgrNhaCC.Rows[nVitri].Selected = true;
            }
            catch
            {
                MessageBox.Show("Có lỗi khi sửa dữ liệu!", "Chú ý");
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (dgrNhaCC.RowCount <= 0)
                return;
            if (MessageBox.Show("Bạn có muốn xóa nhà cung cấp này?", "Chú ý", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;
            try
            {
                if (!gf.fKetNoiCSDL())
                    return;
                using (SqlCommand cmd = new SqlCommand("spXoaNCC", gf.myCnn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@MaCanxoa", dgrNhaCC.CurrentRow.Cells[0].Value.ToString());
                    cmd.ExecuteNonQuery();
                    gf.fHienthiDulieuDatagridView(sView, dgrNhaCC);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi: " + ex.Message, "Thông báo");
            }
        }

        private void dgrNhaCC_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
