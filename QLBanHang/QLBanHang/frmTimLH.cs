﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLBanHang
{
    public partial class frmTimLH : Form
    {
        GlobalFuncion gf = new GlobalFuncion();
        string sView = "vLoaiHang";
        string sTimkiem = "";
        public frmTimLH()
        {
            InitializeComponent();
        }

        private void frmTimLH_Load(object sender, EventArgs e)
        {
            gf.fHienthiDulieuDatagridView(sView, dgrLoaihang);
            txtDieukien.Visible = false;
            sTimkiem = sView;
        }

        private void rbtTatca_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = false;
        }

        private void rbtTimtheoMaL_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = true;
            txtDieukien.Top = rbtTimtheoMaL.Top;
        }

        private void rbtTimtheoTenL_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = true;
            txtDieukien.Top = rbtTimtheoTenL.Top;
        }

        private void btnTim_Click(object sender, EventArgs e)
        {
            string strSQL = sView;
            if (rbtTimtheoMaL.Checked)
                strSQL = strSQL + " WHERE [Mã Loại Hàng]='" + txtDieukien.Text + "'";
            if (rbtTimtheoTenL.Checked)
                strSQL = strSQL + " WHERE [Tên Loại Hàng] like N'%" + txtDieukien.Text + "'";
            gf.fHienthiDulieuDatagridView(strSQL, dgrLoaihang);
            sTimkiem = strSQL;
        }
    }
}
