﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

namespace QLBanHang
{
    public partial class frmDanhsachNV : Form
    {
        GlobalFuncion gf = new GlobalFuncion();
        string strBaocao = "";
        public frmDanhsachNV(string s)
        {
            InitializeComponent();
            strBaocao = s;
        }

        private void frmDanhsachNV_Load(object sender, EventArgs e)
        {
            SqlConnection myCnn = new SqlConnection(@"Data Source=DESKTOP-KBKCTHD;Initial Catalog=QLBH;Integrated Security=True");
            string strSQL = "select * from vNhanvien";
            SqlDataAdapter da = new SqlDataAdapter(strSQL, myCnn);
            dtsNV nv = new dtsNV();
            da.Fill(nv, "vNhanvien");
           rptDSNV rpt = new rptDSNV();
            rpt.SetDataSource(nv);
            crystalReportViewer1.ReportSource = rpt;
            crystalReportViewer1.Refresh();
        }
    }
}
