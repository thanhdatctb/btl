﻿namespace QLBanHang
{
    partial class frmTimPN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgrTimPN = new System.Windows.Forms.DataGridView();
            this.grbTimPN = new System.Windows.Forms.GroupBox();
            this.BtnIn = new System.Windows.Forms.Button();
            this.btnTim = new System.Windows.Forms.Button();
            this.txtDieukien = new System.Windows.Forms.TextBox();
            this.rbtTimtheoNgaynhap = new System.Windows.Forms.RadioButton();
            this.rbtTimtheoSoPN = new System.Windows.Forms.RadioButton();
            this.rbtTatca = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgrTimPN)).BeginInit();
            this.grbTimPN.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgrTimPN
            // 
            this.dgrTimPN.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrTimPN.Location = new System.Drawing.Point(2, 3);
            this.dgrTimPN.Name = "dgrTimPN";
            this.dgrTimPN.Size = new System.Drawing.Size(825, 210);
            this.dgrTimPN.TabIndex = 0;
            // 
            // grbTimPN
            // 
            this.grbTimPN.Controls.Add(this.BtnIn);
            this.grbTimPN.Controls.Add(this.btnTim);
            this.grbTimPN.Controls.Add(this.txtDieukien);
            this.grbTimPN.Controls.Add(this.rbtTimtheoNgaynhap);
            this.grbTimPN.Controls.Add(this.rbtTimtheoSoPN);
            this.grbTimPN.Controls.Add(this.rbtTatca);
            this.grbTimPN.Location = new System.Drawing.Point(2, 235);
            this.grbTimPN.Name = "grbTimPN";
            this.grbTimPN.Size = new System.Drawing.Size(814, 187);
            this.grbTimPN.TabIndex = 1;
            this.grbTimPN.TabStop = false;
            this.grbTimPN.Text = "Chọn tiêu chí";
            // 
            // BtnIn
            // 
            this.BtnIn.Location = new System.Drawing.Point(575, 116);
            this.BtnIn.Name = "BtnIn";
            this.BtnIn.Size = new System.Drawing.Size(176, 23);
            this.BtnIn.TabIndex = 6;
            this.BtnIn.Text = "Xem danh sách phiếu nhập";
            this.BtnIn.UseVisualStyleBackColor = true;
            // 
            // btnTim
            // 
            this.btnTim.Location = new System.Drawing.Point(575, 39);
            this.btnTim.Name = "btnTim";
            this.btnTim.Size = new System.Drawing.Size(176, 23);
            this.btnTim.TabIndex = 5;
            this.btnTim.Text = "Tìm";
            this.btnTim.UseVisualStyleBackColor = true;
            this.btnTim.Click += new System.EventHandler(this.btnTim_Click);
            // 
            // txtDieukien
            // 
            this.txtDieukien.Location = new System.Drawing.Point(203, 67);
            this.txtDieukien.Name = "txtDieukien";
            this.txtDieukien.Size = new System.Drawing.Size(163, 20);
            this.txtDieukien.TabIndex = 4;
            // 
            // rbtTimtheoNgaynhap
            // 
            this.rbtTimtheoNgaynhap.AutoSize = true;
            this.rbtTimtheoNgaynhap.Location = new System.Drawing.Point(30, 122);
            this.rbtTimtheoNgaynhap.Name = "rbtTimtheoNgaynhap";
            this.rbtTimtheoNgaynhap.Size = new System.Drawing.Size(119, 17);
            this.rbtTimtheoNgaynhap.TabIndex = 2;
            this.rbtTimtheoNgaynhap.TabStop = true;
            this.rbtTimtheoNgaynhap.Text = "Tìm theo ngày nhập";
            this.rbtTimtheoNgaynhap.UseVisualStyleBackColor = true;
            this.rbtTimtheoNgaynhap.CheckedChanged += new System.EventHandler(this.rbtTimtheoNgaynhap_CheckedChanged);
            // 
            // rbtTimtheoSoPN
            // 
            this.rbtTimtheoSoPN.AutoSize = true;
            this.rbtTimtheoSoPN.Location = new System.Drawing.Point(30, 70);
            this.rbtTimtheoSoPN.Name = "rbtTimtheoSoPN";
            this.rbtTimtheoSoPN.Size = new System.Drawing.Size(136, 17);
            this.rbtTimtheoSoPN.TabIndex = 1;
            this.rbtTimtheoSoPN.TabStop = true;
            this.rbtTimtheoSoPN.Text = "Tìm theo số phiếu nhập";
            this.rbtTimtheoSoPN.UseVisualStyleBackColor = true;
            this.rbtTimtheoSoPN.CheckedChanged += new System.EventHandler(this.rbtTimtheoSoPN_CheckedChanged);
            // 
            // rbtTatca
            // 
            this.rbtTatca.AutoSize = true;
            this.rbtTatca.Location = new System.Drawing.Point(30, 20);
            this.rbtTatca.Name = "rbtTatca";
            this.rbtTatca.Size = new System.Drawing.Size(77, 17);
            this.rbtTatca.TabIndex = 0;
            this.rbtTatca.TabStop = true;
            this.rbtTatca.Text = "Hiện tất cả";
            this.rbtTatca.UseVisualStyleBackColor = true;
            this.rbtTatca.CheckedChanged += new System.EventHandler(this.rbtTatca_CheckedChanged);
            // 
            // frmTimPN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 425);
            this.Controls.Add(this.grbTimPN);
            this.Controls.Add(this.dgrTimPN);
            this.Name = "frmTimPN";
            this.Text = "frmTimPN";
            this.Load += new System.EventHandler(this.frmTimPN_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgrTimPN)).EndInit();
            this.grbTimPN.ResumeLayout(false);
            this.grbTimPN.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgrTimPN;
        private System.Windows.Forms.GroupBox grbTimPN;
        private System.Windows.Forms.Button BtnIn;
        private System.Windows.Forms.Button btnTim;
        private System.Windows.Forms.TextBox txtDieukien;
        private System.Windows.Forms.RadioButton rbtTimtheoNgaynhap;
        private System.Windows.Forms.RadioButton rbtTimtheoSoPN;
        private System.Windows.Forms.RadioButton rbtTatca;
    }
}