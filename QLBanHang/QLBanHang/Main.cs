﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLBanHang
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void đăngNhậpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmDangnhap f = new frmDangnhap();
            this.Hide();
            f.Show();
        }

        private void thoátToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn thoát hay không  ", "Thông Báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) == DialogResult.OK)
            {
                this.Close();
            } 
        }

        private void loạiHàngToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmLoaihang f = new frmLoaihang();
            f.MdiParent = this;
            f.Show();
        }

        private void tìmKiếmLoạiHàngToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTimLH f = new frmTimLH();
            f.Show();
        }

        private void tìmKiếmHàngHóaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTimHH f = new frmTimHH();
            f.Show();
        }

        private void hàngHóaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmHanghoa f = new frmHanghoa();
            f.Show();
        }

        private void kháchHàngToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmKhachhang f = new frmKhachhang();
            f.Show();
        }

        private void nhàCungCấpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmNhacungcap f = new frmNhacungcap();
            f.Show();
        }

        private void nhânViênToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmNhanvien f = new frmNhanvien();
            f.Show();
        }

        private void hóaĐơnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmHoadon f = new frmHoadon();
            f.Show();
        }

        private void phiếuNhậpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPhieunhap f = new frmPhieunhap();
            f.Show();
        }

        private void tìmKiếmNhânViênToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTimkiemNV f = new frmTimkiemNV();
            f.Show();
        }

        private void tìmKiếmKháchHàngToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTimKH f = new frmTimKH();
            f.Show();
        }

        private void tìmNhàCungCấpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTimNCC f = new frmTimNCC();
            f.Show();
        }

        private void tìmHóaĐơnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmTimHD f = new FrmTimHD();
            f.Show();
        }

        private void tìmPhiếuNhậpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTimPN f = new frmTimPN();
            f.Show();
        }
    }
}
