﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QLBanHang
{
    public partial class frmTimNCC : Form
    {
        GlobalFuncion gf = new GlobalFuncion();
        string sView = "vNhanvien";
        string sTimkiem = "";
        public frmTimNCC()
        {
            InitializeComponent();
        }

        private void frmTimNCC_Load(object sender, EventArgs e)
        {
            gf.fHienthiDulieuDatagridView(sView, dgrTimNCC);
            txtDieukien.Visible = false;
            sTimkiem = sView;
        }

        private void rbtTatca_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = false;
        }

        private void rbtTimtheoMaNCC_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = true;
            txtDieukien.Top = rbtTimtheoMaNCC.Top;
        }

        private void rbtTimtheoTen_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = true;
            txtDieukien.Top = rbtTimtheoTen.Top;
        }

        private void rbtTimDC_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = true;
            txtDieukien.Top = rbtTimDC.Top;
        }

        private void btnTim_Click(object sender, EventArgs e)
        {
            string strSQL = sView;
            if (rbtTimtheoMaNCC.Checked)
                strSQL = strSQL + " WHERE [Mã Nhà Cung Cấp]='" + txtDieukien.Text + "'";
            if (rbtTimtheoTen.Checked)
                strSQL = strSQL + " WHERE [Tên Nhà Cung Cấp] like N'%" + txtDieukien.Text + "'";
            if (rbtTimDC.Checked)
                strSQL = strSQL + " WHERE [Địa Chỉ] like N'%" + txtDieukien.Text + "'";
            gf.fHienthiDulieuDatagridView(strSQL, dgrTimNCC);
            sTimkiem = strSQL;
        }
    }
}
