﻿namespace QLBanHang
{
    partial class frmTimHH
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgrTimHH = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboDVT = new System.Windows.Forms.ComboBox();
            this.btnIn = new System.Windows.Forms.Button();
            this.btnTim = new System.Windows.Forms.Button();
            this.txtDieukien = new System.Windows.Forms.TextBox();
            this.rbtTimtheoTen = new System.Windows.Forms.RadioButton();
            this.rbtTimtheoDVT = new System.Windows.Forms.RadioButton();
            this.rbtTimtheoMaHH = new System.Windows.Forms.RadioButton();
            this.rbtTatca = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgrTimHH)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgrTimHH
            // 
            this.dgrTimHH.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrTimHH.Location = new System.Drawing.Point(16, 0);
            this.dgrTimHH.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgrTimHH.Name = "dgrTimHH";
            this.dgrTimHH.RowHeadersWidth = 51;
            this.dgrTimHH.Size = new System.Drawing.Size(729, 185);
            this.dgrTimHH.TabIndex = 0;
            this.dgrTimHH.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgrTimHH_CellContentClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboDVT);
            this.groupBox1.Controls.Add(this.btnIn);
            this.groupBox1.Controls.Add(this.btnTim);
            this.groupBox1.Controls.Add(this.txtDieukien);
            this.groupBox1.Controls.Add(this.rbtTimtheoTen);
            this.groupBox1.Controls.Add(this.rbtTimtheoDVT);
            this.groupBox1.Controls.Add(this.rbtTimtheoMaHH);
            this.groupBox1.Controls.Add(this.rbtTatca);
            this.groupBox1.Location = new System.Drawing.Point(16, 192);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(729, 203);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Chọn tiêu chí";
            // 
            // cboDVT
            // 
            this.cboDVT.FormattingEnabled = true;
            this.cboDVT.Items.AddRange(new object[] {
            "Goi",
            "Chai",
            "Vi",
            "Hop"});
            this.cboDVT.Location = new System.Drawing.Point(211, 151);
            this.cboDVT.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboDVT.Name = "cboDVT";
            this.cboDVT.Size = new System.Drawing.Size(160, 24);
            this.cboDVT.TabIndex = 4;
            // 
            // btnIn
            // 
            this.btnIn.Location = new System.Drawing.Point(573, 121);
            this.btnIn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnIn.Name = "btnIn";
            this.btnIn.Size = new System.Drawing.Size(100, 28);
            this.btnIn.TabIndex = 3;
            this.btnIn.Text = "In";
            this.btnIn.UseVisualStyleBackColor = true;
            // 
            // btnTim
            // 
            this.btnTim.Location = new System.Drawing.Point(573, 39);
            this.btnTim.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTim.Name = "btnTim";
            this.btnTim.Size = new System.Drawing.Size(100, 28);
            this.btnTim.TabIndex = 3;
            this.btnTim.Text = "Tìm";
            this.btnTim.UseVisualStyleBackColor = true;
            this.btnTim.Click += new System.EventHandler(this.btnTim_Click);
            // 
            // txtDieukien
            // 
            this.txtDieukien.Location = new System.Drawing.Point(211, 63);
            this.txtDieukien.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDieukien.Name = "txtDieukien";
            this.txtDieukien.Size = new System.Drawing.Size(160, 22);
            this.txtDieukien.TabIndex = 1;
            // 
            // rbtTimtheoTen
            // 
            this.rbtTimtheoTen.AutoSize = true;
            this.rbtTimtheoTen.Location = new System.Drawing.Point(4, 110);
            this.rbtTimtheoTen.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbtTimtheoTen.Name = "rbtTimtheoTen";
            this.rbtTimtheoTen.Size = new System.Drawing.Size(108, 21);
            this.rbtTimtheoTen.TabIndex = 0;
            this.rbtTimtheoTen.Text = "Tìm theo tên";
            this.rbtTimtheoTen.UseVisualStyleBackColor = true;
            this.rbtTimtheoTen.CheckedChanged += new System.EventHandler(this.rbtTimtheoTen_CheckedChanged);
            // 
            // rbtTimtheoDVT
            // 
            this.rbtTimtheoDVT.AutoSize = true;
            this.rbtTimtheoDVT.Location = new System.Drawing.Point(4, 156);
            this.rbtTimtheoDVT.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbtTimtheoDVT.Name = "rbtTimtheoDVT";
            this.rbtTimtheoDVT.Size = new System.Drawing.Size(155, 21);
            this.rbtTimtheoDVT.TabIndex = 0;
            this.rbtTimtheoDVT.Text = "Tìm theo Đơn vị tính";
            this.rbtTimtheoDVT.UseVisualStyleBackColor = true;
            this.rbtTimtheoDVT.CheckedChanged += new System.EventHandler(this.rbtTimtheoDVT_CheckedChanged);
            // 
            // rbtTimtheoMaHH
            // 
            this.rbtTimtheoMaHH.AutoSize = true;
            this.rbtTimtheoMaHH.Location = new System.Drawing.Point(4, 63);
            this.rbtTimtheoMaHH.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbtTimtheoMaHH.Name = "rbtTimtheoMaHH";
            this.rbtTimtheoMaHH.Size = new System.Drawing.Size(171, 21);
            this.rbtTimtheoMaHH.TabIndex = 0;
            this.rbtTimtheoMaHH.Text = "Tìm theo Mã hàng hóa";
            this.rbtTimtheoMaHH.UseVisualStyleBackColor = true;
            this.rbtTimtheoMaHH.CheckedChanged += new System.EventHandler(this.rbtTimtheoMaHH_CheckedChanged);
            // 
            // rbtTatca
            // 
            this.rbtTatca.AutoSize = true;
            this.rbtTatca.Checked = true;
            this.rbtTatca.Location = new System.Drawing.Point(4, 20);
            this.rbtTatca.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbtTatca.Name = "rbtTatca";
            this.rbtTatca.Size = new System.Drawing.Size(97, 21);
            this.rbtTatca.TabIndex = 0;
            this.rbtTatca.TabStop = true;
            this.rbtTatca.Text = "Hiện tất cả";
            this.rbtTatca.UseVisualStyleBackColor = true;
            this.rbtTatca.CheckedChanged += new System.EventHandler(this.rbtTatca_CheckedChanged);
            // 
            // frmTimHH
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(757, 399);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgrTimHH);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmTimHH";
            this.Text = "frmTimHH";
            this.Load += new System.EventHandler(this.frmTimHH_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgrTimHH)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgrTimHH;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnIn;
        private System.Windows.Forms.Button btnTim;
        private System.Windows.Forms.TextBox txtDieukien;
        private System.Windows.Forms.RadioButton rbtTimtheoTen;
        private System.Windows.Forms.RadioButton rbtTimtheoDVT;
        private System.Windows.Forms.RadioButton rbtTimtheoMaHH;
        private System.Windows.Forms.RadioButton rbtTatca;
        private System.Windows.Forms.ComboBox cboDVT;
    }
}