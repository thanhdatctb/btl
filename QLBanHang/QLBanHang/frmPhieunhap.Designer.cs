﻿namespace QLBanHang
{
    partial class frmPhieunhap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboTenHH = new System.Windows.Forms.ComboBox();
            this.txtGN = new System.Windows.Forms.TextBox();
            this.txtSL = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnThemSP = new System.Windows.Forms.Button();
            this.dgrChitietPN = new System.Windows.Forms.DataGridView();
            this.cboNCC = new System.Windows.Forms.ComboBox();
            this.cboNV = new System.Windows.Forms.ComboBox();
            this.btnSua = new System.Windows.Forms.Button();
            this.btnThem = new System.Windows.Forms.Button();
            this.dgrPhieunhap = new System.Windows.Forms.DataGridView();
            this.txtNgaylap = new System.Windows.Forms.TextBox();
            this.txtSoPN = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.rbtOK = new System.Windows.Forms.RadioButton();
            this.rbtChua = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgrChitietPN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgrPhieunhap)).BeginInit();
            this.SuspendLayout();
            // 
            // cboTenHH
            // 
            this.cboTenHH.FormattingEnabled = true;
            this.cboTenHH.Location = new System.Drawing.Point(518, 234);
            this.cboTenHH.Name = "cboTenHH";
            this.cboTenHH.Size = new System.Drawing.Size(132, 21);
            this.cboTenHH.TabIndex = 31;
            // 
            // txtGN
            // 
            this.txtGN.Location = new System.Drawing.Point(518, 320);
            this.txtGN.Name = "txtGN";
            this.txtGN.Size = new System.Drawing.Size(132, 20);
            this.txtGN.TabIndex = 30;
            // 
            // txtSL
            // 
            this.txtSL.Location = new System.Drawing.Point(518, 279);
            this.txtSL.Name = "txtSL";
            this.txtSL.Size = new System.Drawing.Size(132, 20);
            this.txtSL.TabIndex = 29;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(418, 282);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 28;
            this.label9.Text = "Số lượng";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(418, 323);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 27;
            this.label8.Text = "Giá nhập";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(418, 234);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Tên hàng hóa";
            // 
            // btnThemSP
            // 
            this.btnThemSP.Location = new System.Drawing.Point(688, 234);
            this.btnThemSP.Name = "btnThemSP";
            this.btnThemSP.Size = new System.Drawing.Size(97, 23);
            this.btnThemSP.TabIndex = 25;
            this.btnThemSP.Text = "Thêm hàng hóa";
            this.btnThemSP.UseVisualStyleBackColor = true;
            this.btnThemSP.Click += new System.EventHandler(this.btnThemSP_Click);
            // 
            // dgrChitietPN
            // 
            this.dgrChitietPN.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrChitietPN.Location = new System.Drawing.Point(51, 209);
            this.dgrChitietPN.Name = "dgrChitietPN";
            this.dgrChitietPN.Size = new System.Drawing.Size(338, 144);
            this.dgrChitietPN.TabIndex = 24;
            // 
            // cboNCC
            // 
            this.cboNCC.FormattingEnabled = true;
            this.cboNCC.Location = new System.Drawing.Point(141, 111);
            this.cboNCC.Name = "cboNCC";
            this.cboNCC.Size = new System.Drawing.Size(121, 21);
            this.cboNCC.TabIndex = 23;
            // 
            // cboNV
            // 
            this.cboNV.FormattingEnabled = true;
            this.cboNV.Location = new System.Drawing.Point(141, 47);
            this.cboNV.Name = "cboNV";
            this.cboNV.Size = new System.Drawing.Size(121, 21);
            this.cboNV.TabIndex = 22;
            // 
            // btnSua
            // 
            this.btnSua.Location = new System.Drawing.Point(581, 119);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(107, 23);
            this.btnSua.TabIndex = 21;
            this.btnSua.Text = "Sửa";
            this.btnSua.UseVisualStyleBackColor = true;
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(395, 119);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(107, 23);
            this.btnThem.TabIndex = 20;
            this.btnThem.Text = "Lập hóa đơn";
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // dgrPhieunhap
            // 
            this.dgrPhieunhap.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrPhieunhap.Location = new System.Drawing.Point(316, 12);
            this.dgrPhieunhap.Name = "dgrPhieunhap";
            this.dgrPhieunhap.Size = new System.Drawing.Size(469, 75);
            this.dgrPhieunhap.TabIndex = 19;
            // 
            // txtNgaylap
            // 
            this.txtNgaylap.Location = new System.Drawing.Point(140, 79);
            this.txtNgaylap.Name = "txtNgaylap";
            this.txtNgaylap.Size = new System.Drawing.Size(122, 20);
            this.txtNgaylap.TabIndex = 17;
            // 
            // txtSoPN
            // 
            this.txtSoPN.Location = new System.Drawing.Point(141, 12);
            this.txtSoPN.Name = "txtSoPN";
            this.txtSoPN.Size = new System.Drawing.Size(121, 20);
            this.txtSoPN.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(49, 149);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Thanh toán";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(49, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Nhà cung cấp";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(49, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Nhân viên nhập";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(49, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Ngày lập";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Số phiếu nhập";
            // 
            // rbtOK
            // 
            this.rbtOK.AutoSize = true;
            this.rbtOK.Location = new System.Drawing.Point(141, 145);
            this.rbtOK.Name = "rbtOK";
            this.rbtOK.Size = new System.Drawing.Size(93, 17);
            this.rbtOK.TabIndex = 32;
            this.rbtOK.TabStop = true;
            this.rbtOK.Text = "Đã thanh toán";
            this.rbtOK.UseVisualStyleBackColor = true;
            // 
            // rbtChua
            // 
            this.rbtChua.AutoSize = true;
            this.rbtChua.Location = new System.Drawing.Point(240, 145);
            this.rbtChua.Name = "rbtChua";
            this.rbtChua.Size = new System.Drawing.Size(50, 17);
            this.rbtChua.TabIndex = 32;
            this.rbtChua.TabStop = true;
            this.rbtChua.Text = "Chưa";
            this.rbtChua.UseVisualStyleBackColor = true;
            // 
            // frmPhieunhap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 365);
            this.Controls.Add(this.rbtChua);
            this.Controls.Add(this.rbtOK);
            this.Controls.Add(this.cboTenHH);
            this.Controls.Add(this.txtGN);
            this.Controls.Add(this.txtSL);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnThemSP);
            this.Controls.Add(this.dgrChitietPN);
            this.Controls.Add(this.cboNCC);
            this.Controls.Add(this.cboNV);
            this.Controls.Add(this.btnSua);
            this.Controls.Add(this.btnThem);
            this.Controls.Add(this.dgrPhieunhap);
            this.Controls.Add(this.txtNgaylap);
            this.Controls.Add(this.txtSoPN);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmPhieunhap";
            this.Text = "frmPhieunhap";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPhieunhap_FormClosing);
            this.Load += new System.EventHandler(this.frmPhieunhap_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgrChitietPN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgrPhieunhap)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboTenHH;
        private System.Windows.Forms.TextBox txtGN;
        private System.Windows.Forms.TextBox txtSL;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnThemSP;
        private System.Windows.Forms.DataGridView dgrChitietPN;
        private System.Windows.Forms.ComboBox cboNCC;
        private System.Windows.Forms.ComboBox cboNV;
        private System.Windows.Forms.Button btnSua;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.DataGridView dgrPhieunhap;
        private System.Windows.Forms.TextBox txtNgaylap;
        private System.Windows.Forms.TextBox txtSoPN;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbtOK;
        private System.Windows.Forms.RadioButton rbtChua;
    }
}