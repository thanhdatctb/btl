﻿namespace QLBanHang
{
    partial class frmTimLH
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgrLoaihang = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnIn = new System.Windows.Forms.Button();
            this.btnTim = new System.Windows.Forms.Button();
            this.txtDieukien = new System.Windows.Forms.TextBox();
            this.rbtTimtheoTenL = new System.Windows.Forms.RadioButton();
            this.rbtTimtheoMaL = new System.Windows.Forms.RadioButton();
            this.rbtTatca = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgrLoaihang)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgrLoaihang
            // 
            this.dgrLoaihang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrLoaihang.Location = new System.Drawing.Point(12, 12);
            this.dgrLoaihang.Name = "dgrLoaihang";
            this.dgrLoaihang.Size = new System.Drawing.Size(285, 150);
            this.dgrLoaihang.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnIn);
            this.groupBox1.Controls.Add(this.btnTim);
            this.groupBox1.Controls.Add(this.txtDieukien);
            this.groupBox1.Controls.Add(this.rbtTimtheoTenL);
            this.groupBox1.Controls.Add(this.rbtTimtheoMaL);
            this.groupBox1.Controls.Add(this.rbtTatca);
            this.groupBox1.Location = new System.Drawing.Point(303, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(344, 133);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Chọn tiêu chí";
            // 
            // btnIn
            // 
            this.btnIn.Location = new System.Drawing.Point(252, 87);
            this.btnIn.Name = "btnIn";
            this.btnIn.Size = new System.Drawing.Size(75, 24);
            this.btnIn.TabIndex = 2;
            this.btnIn.Text = "In";
            this.btnIn.UseVisualStyleBackColor = true;
            // 
            // btnTim
            // 
            this.btnTim.Location = new System.Drawing.Point(252, 29);
            this.btnTim.Name = "btnTim";
            this.btnTim.Size = new System.Drawing.Size(75, 28);
            this.btnTim.TabIndex = 2;
            this.btnTim.Text = "Tìm";
            this.btnTim.UseVisualStyleBackColor = true;
            this.btnTim.Click += new System.EventHandler(this.btnTim_Click);
            // 
            // txtDieukien
            // 
            this.txtDieukien.Location = new System.Drawing.Point(158, 53);
            this.txtDieukien.Name = "txtDieukien";
            this.txtDieukien.Size = new System.Drawing.Size(65, 20);
            this.txtDieukien.TabIndex = 1;
            // 
            // rbtTimtheoTenL
            // 
            this.rbtTimtheoTenL.AutoSize = true;
            this.rbtTimtheoTenL.Location = new System.Drawing.Point(22, 94);
            this.rbtTimtheoTenL.Name = "rbtTimtheoTenL";
            this.rbtTimtheoTenL.Size = new System.Drawing.Size(134, 17);
            this.rbtTimtheoTenL.TabIndex = 0;
            this.rbtTimtheoTenL.Text = "Tìm theo Tên loại hàng";
            this.rbtTimtheoTenL.UseVisualStyleBackColor = true;
            this.rbtTimtheoTenL.CheckedChanged += new System.EventHandler(this.rbtTimtheoTenL_CheckedChanged);
            // 
            // rbtTimtheoMaL
            // 
            this.rbtTimtheoMaL.AutoSize = true;
            this.rbtTimtheoMaL.Location = new System.Drawing.Point(22, 56);
            this.rbtTimtheoMaL.Name = "rbtTimtheoMaL";
            this.rbtTimtheoMaL.Size = new System.Drawing.Size(130, 17);
            this.rbtTimtheoMaL.TabIndex = 0;
            this.rbtTimtheoMaL.Text = "Tìm theo Mã loại hàng";
            this.rbtTimtheoMaL.UseVisualStyleBackColor = true;
            this.rbtTimtheoMaL.CheckedChanged += new System.EventHandler(this.rbtTimtheoMaL_CheckedChanged);
            // 
            // rbtTatca
            // 
            this.rbtTatca.AutoSize = true;
            this.rbtTatca.Checked = true;
            this.rbtTatca.Location = new System.Drawing.Point(22, 19);
            this.rbtTatca.Name = "rbtTatca";
            this.rbtTatca.Size = new System.Drawing.Size(77, 17);
            this.rbtTatca.TabIndex = 0;
            this.rbtTatca.TabStop = true;
            this.rbtTatca.Text = "Hiện tất cả";
            this.rbtTatca.UseVisualStyleBackColor = true;
            this.rbtTatca.CheckedChanged += new System.EventHandler(this.rbtTatca_CheckedChanged);
            // 
            // frmTimLH
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 162);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgrLoaihang);
            this.Name = "frmTimLH";
            this.Text = "frmTimLH";
            this.Load += new System.EventHandler(this.frmTimLH_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgrLoaihang)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgrLoaihang;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnIn;
        private System.Windows.Forms.Button btnTim;
        private System.Windows.Forms.TextBox txtDieukien;
        private System.Windows.Forms.RadioButton rbtTimtheoTenL;
        private System.Windows.Forms.RadioButton rbtTimtheoMaL;
        private System.Windows.Forms.RadioButton rbtTatca;
    }
}