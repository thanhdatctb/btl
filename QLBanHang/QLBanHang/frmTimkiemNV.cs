﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QLBanHang
{
    public partial class frmTimkiemNV : Form
    {
        GlobalFuncion gf = new GlobalFuncion();
        string sView = "vNhanvien";
        string sTimkiem = "";
        public frmTimkiemNV()
        {
            InitializeComponent();
        }

        private void frmTimkiemNV_Load(object sender, EventArgs e)
        {
            gf.fHienthiDulieuDatagridView(sView, dgrTimNV);
            txtDieukien.Visible = false;
            sTimkiem = sView;
        }

        private void rbtTatca_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = false;
        }

        private void rbtTimtheoMaNV_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = true;
            txtDieukien.Top = rbtTimtheoMaNV.Top;
        }

        private void rbtTimtheoTen_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = true;
            txtDieukien.Top = rbtTimtheoTen.Top;
        }

        private void rbtTimMaDC_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = true;
            txtDieukien.Top = rbtTimDC.Top;
        }

      
        private void btnTim_Click(object sender, EventArgs e)
        {
            string strSQL = sView;
            if (rbtTimtheoMaNV.Checked)
                strSQL = strSQL + " WHERE [Mã nhân viên]='" + txtDieukien.Text + "'";
            if (rbtTimtheoTen.Checked)
                strSQL = strSQL + " WHERE [Tên nhân viên] like N'%" + txtDieukien.Text + "%'";
            if (rbtTimDC.Checked)
                strSQL = strSQL + " WHERE [Địa chỉ] like N'%" + txtDieukien.Text + "'";
          

            gf.fHienthiDulieuDatagridView(strSQL, dgrTimNV);
            sTimkiem = strSQL;
        }

        private void btnIn_Click(object sender, EventArgs e)
        {
            frmDanhsachNV rp = new frmDanhsachNV(sTimkiem);
            rp.Show();
        }

     

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

      
    }
}
