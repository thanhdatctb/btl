﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QLBanHang
{
    public partial class frmTimKH : Form
    {
        GlobalFuncion gf = new GlobalFuncion();
        string sView = "vKhachhang";
        string sTimkiem = "";
        public frmTimKH()
        {
            InitializeComponent();
        }

        private void rbtTatca_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = false;
        }

        private void rbtTimtheoMaKH_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = true;
            txtDieukien.Top = rbtTimtheoMaKH.Top;
        }

        private void frmTimKH_Load(object sender, EventArgs e)
        {
            gf.fHienthiDulieuDatagridView(sView, dgrTimKH);
            txtDieukien.Visible = false;
            sTimkiem = sView;
        }

        private void rbtTimtheoTen_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = true;
            txtDieukien.Top = rbtTimtheoTen.Top;
        }

        private void rbtTimDC_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = true;
            txtDieukien.Top = rbtTimDC.Top;
        }

        private void btnTim_Click(object sender, EventArgs e)
        {
            string strSQL = sView;
            if (rbtTimtheoMaKH.Checked)
                strSQL = strSQL + " WHERE [Mã Khách Hàng]='" + txtDieukien.Text + "'";
            if (rbtTimtheoTen.Checked)
                strSQL = strSQL + " WHERE [Tên Khách Hàng] like N'%" + txtDieukien.Text + "'";
            if (rbtTimDC.Checked)
                strSQL = strSQL + " WHERE [Địa chỉ] like N'%" + txtDieukien.Text + "'";
            gf.fHienthiDulieuDatagridView(strSQL, dgrTimKH);
            sTimkiem = strSQL;
        }

        private void dgrTimKH_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
