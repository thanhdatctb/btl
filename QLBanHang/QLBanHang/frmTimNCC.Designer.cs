﻿namespace QLBanHang
{
    partial class frmTimNCC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnIn = new System.Windows.Forms.Button();
            this.btnTim = new System.Windows.Forms.Button();
            this.txtDieukien = new System.Windows.Forms.TextBox();
            this.rbtTimtheoTen = new System.Windows.Forms.RadioButton();
            this.rbtTimDC = new System.Windows.Forms.RadioButton();
            this.rbtTimtheoMaNCC = new System.Windows.Forms.RadioButton();
            this.rbtTatca = new System.Windows.Forms.RadioButton();
            this.dgrTimNCC = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrTimNCC)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnIn);
            this.groupBox1.Controls.Add(this.btnTim);
            this.groupBox1.Controls.Add(this.txtDieukien);
            this.groupBox1.Controls.Add(this.rbtTimtheoTen);
            this.groupBox1.Controls.Add(this.rbtTimDC);
            this.groupBox1.Controls.Add(this.rbtTimtheoMaNCC);
            this.groupBox1.Controls.Add(this.rbtTatca);
            this.groupBox1.Location = new System.Drawing.Point(12, 168);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(535, 179);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Chọn tiêu chí";
            // 
            // btnIn
            // 
            this.btnIn.Location = new System.Drawing.Point(430, 104);
            this.btnIn.Name = "btnIn";
            this.btnIn.Size = new System.Drawing.Size(75, 23);
            this.btnIn.TabIndex = 3;
            this.btnIn.Text = "In";
            this.btnIn.UseVisualStyleBackColor = true;
            // 
            // btnTim
            // 
            this.btnTim.Location = new System.Drawing.Point(430, 45);
            this.btnTim.Name = "btnTim";
            this.btnTim.Size = new System.Drawing.Size(75, 23);
            this.btnTim.TabIndex = 3;
            this.btnTim.Text = "Tìm";
            this.btnTim.UseVisualStyleBackColor = true;
            this.btnTim.Click += new System.EventHandler(this.btnTim_Click);
            // 
            // txtDieukien
            // 
            this.txtDieukien.Location = new System.Drawing.Point(180, 86);
            this.txtDieukien.Name = "txtDieukien";
            this.txtDieukien.Size = new System.Drawing.Size(100, 20);
            this.txtDieukien.TabIndex = 1;
            // 
            // rbtTimtheoTen
            // 
            this.rbtTimtheoTen.AutoSize = true;
            this.rbtTimtheoTen.Location = new System.Drawing.Point(3, 89);
            this.rbtTimtheoTen.Name = "rbtTimtheoTen";
            this.rbtTimtheoTen.Size = new System.Drawing.Size(84, 17);
            this.rbtTimtheoTen.TabIndex = 0;
            this.rbtTimtheoTen.Text = "Tìm theo tên";
            this.rbtTimtheoTen.UseVisualStyleBackColor = true;
            this.rbtTimtheoTen.CheckedChanged += new System.EventHandler(this.rbtTimtheoTen_CheckedChanged);
            // 
            // rbtTimDC
            // 
            this.rbtTimDC.AutoSize = true;
            this.rbtTimDC.Location = new System.Drawing.Point(3, 126);
            this.rbtTimDC.Name = "rbtTimDC";
            this.rbtTimDC.Size = new System.Drawing.Size(101, 17);
            this.rbtTimDC.TabIndex = 0;
            this.rbtTimDC.Text = "Tìm theo địa chỉ";
            this.rbtTimDC.UseVisualStyleBackColor = true;
            this.rbtTimDC.CheckedChanged += new System.EventHandler(this.rbtTimDC_CheckedChanged);
            // 
            // rbtTimtheoMaNCC
            // 
            this.rbtTimtheoMaNCC.AutoSize = true;
            this.rbtTimtheoMaNCC.Location = new System.Drawing.Point(3, 51);
            this.rbtTimtheoMaNCC.Name = "rbtTimtheoMaNCC";
            this.rbtTimtheoMaNCC.Size = new System.Drawing.Size(153, 17);
            this.rbtTimtheoMaNCC.TabIndex = 0;
            this.rbtTimtheoMaNCC.Text = "Tìm theo Mã nhà cung cấp";
            this.rbtTimtheoMaNCC.UseVisualStyleBackColor = true;
            this.rbtTimtheoMaNCC.CheckedChanged += new System.EventHandler(this.rbtTimtheoMaNCC_CheckedChanged);
            // 
            // rbtTatca
            // 
            this.rbtTatca.AutoSize = true;
            this.rbtTatca.Checked = true;
            this.rbtTatca.Location = new System.Drawing.Point(3, 16);
            this.rbtTatca.Name = "rbtTatca";
            this.rbtTatca.Size = new System.Drawing.Size(77, 17);
            this.rbtTatca.TabIndex = 0;
            this.rbtTatca.TabStop = true;
            this.rbtTatca.Text = "Hiện tất cả";
            this.rbtTatca.UseVisualStyleBackColor = true;
            this.rbtTatca.CheckedChanged += new System.EventHandler(this.rbtTatca_CheckedChanged);
            // 
            // dgrTimNCC
            // 
            this.dgrTimNCC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrTimNCC.Location = new System.Drawing.Point(12, 2);
            this.dgrTimNCC.Name = "dgrTimNCC";
            this.dgrTimNCC.Size = new System.Drawing.Size(547, 150);
            this.dgrTimNCC.TabIndex = 4;
            // 
            // frmTimNCC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 362);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgrTimNCC);
            this.Name = "frmTimNCC";
            this.Text = "frmTimNCC";
            this.Load += new System.EventHandler(this.frmTimNCC_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrTimNCC)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnIn;
        private System.Windows.Forms.Button btnTim;
        private System.Windows.Forms.TextBox txtDieukien;
        private System.Windows.Forms.RadioButton rbtTimtheoTen;
        private System.Windows.Forms.RadioButton rbtTimDC;
        private System.Windows.Forms.RadioButton rbtTimtheoMaNCC;
        private System.Windows.Forms.RadioButton rbtTatca;
        private System.Windows.Forms.DataGridView dgrTimNCC;
    }
}