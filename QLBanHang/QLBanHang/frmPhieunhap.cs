﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace QLBanHang
{
    public partial class frmPhieunhap : Form
    {
        GlobalFuncion gf = new GlobalFuncion();
        public frmPhieunhap()
        {
            InitializeComponent();
        }

        private void frmPhieunhap_Load(object sender, EventArgs e)
        {
            gf.fHienthiDulieuComboBox("tblNhanVien", "TenNV", "MaNV", cboNV);
            gf.fHienthiDulieuComboBox("tblNhaCungCap", "TenNCC", "MaNCC", cboNCC);
            gf.fHienthiDulieuComboBox("tblHangHoa", "TenH", "MaH", cboTenHH);
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if (gf.fKiemtraXaurong(txtSoPN.Text) == true || gf.fKiemtraXaurong(txtNgaylap.Text) == true)
            {
                MessageBox.Show("Chưa nhập đủ thông tin phiếu nhập", "Chú ý");
                return;
            }
            if (gf.fKiemtraTrungkhoa("tblPhieuNhap", "SoPN", txtSoPN.Text) == true)
            {
                MessageBox.Show("Mã phiếu nhập này đã có!", "Chú ý");
                return;
            }
            string MaNCC, MaNV;
            MaNCC = cboNCC.SelectedValue.ToString();
            MaNV = cboNV.SelectedValue.ToString();
            try
            {
                if (gf.fKetNoiCSDL() == false)
                    return;
                SqlCommand cmd = new SqlCommand("Ins_PhieuNhap", gf.myCnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SoPN", txtSoPN.Text);
                cmd.Parameters.AddWithValue("@MaNV", MaNV);
                cmd.Parameters.AddWithValue("@NgayNhap", DateTime.Parse(txtNgaylap.Text));
                cmd.Parameters.AddWithValue("@MaNCC", MaNCC);
                cmd.Parameters.AddWithValue("@DaThanhToan", rbtOK.Checked == true ? "True" : "False");
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                gf.fHienthiDulieuDatagridView("vPhieunhap where [Số phiếu nhập] ='" + txtSoPN.Text + "'", dgrPhieunhap);
            }
            catch
            {
                MessageBox.Show("Có lỗi khi thêm phiếu nhập!", "Chú ý");
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (gf.fKiemtraXaurong(txtSoPN.Text) == true || gf.fKiemtraXaurong(txtNgaylap.Text) == true)
            {
                MessageBox.Show("Chưa nhập đủ thông tin phiếu nhập", "Chú ý");
                return;
            }
            string MaNCC, MaNV;
            MaNCC = cboNCC.SelectedValue.ToString();
            MaNV = cboNV.SelectedValue.ToString();
            try
            {
                int nVitri = dgrPhieunhap.CurrentRow.Index;
                if (gf.fKetNoiCSDL() == false)
                    return;
                SqlCommand cmd = new SqlCommand("spSuaPN", gf.myCnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SoPN", txtSoPN.Text);
                cmd.Parameters.AddWithValue("@MaNV", MaNV);
                cmd.Parameters.AddWithValue("@Ngaynhap", DateTime.Parse(txtNgaylap.Text));
                cmd.Parameters.AddWithValue("@MaNCC", MaNCC);
                cmd.Parameters.AddWithValue("@Dathanhtoan", rbtOK.Checked == true ? "True" : "False");
               // cmd.Parameters.AddWithValue("@Macansua", dgrHoadon.CurrentRow.Cells[0].Value.ToString());
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                gf.fHienthiDulieuDatagridView("vPhieunhap where [Số phiếu nhập] ='" + txtSoPN.Text + "'", dgrPhieunhap);
                //Chọn dòng vừa sửa
                dgrPhieunhap.ClearSelection();
                dgrPhieunhap.CurrentCell = dgrPhieunhap.Rows[nVitri].Cells[0];
                dgrPhieunhap.Rows[nVitri].Selected = true;
            }
            catch
            {
                MessageBox.Show("Có lỗi khi sửa dữ liệu!", "Chú ý");
            }
        }

        private void frmPhieunhap_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void btnThemSP_Click(object sender, EventArgs e)
        {
            if (gf.fKiemtraXaurong(txtSL.Text) == true || gf.fKiemtraXaurong(txtGN.Text) == true)
            {
                MessageBox.Show("Chưa nhập đủ thông tin hàng hóa", "Chú ý");
                return;
            }
            string MaH;
            MaH = cboTenHH.SelectedValue.ToString();
            try
            {
                if (gf.fKetNoiCSDL() == false)
                    return;
                SqlCommand cmd = new SqlCommand("Ins_ChiTietPN", gf.myCnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SoPN", txtSoPN.Text);
                cmd.Parameters.AddWithValue("@MaH", MaH);
                cmd.Parameters.AddWithValue("@GiaNhap", txtGN.Text);
                cmd.Parameters.AddWithValue("@SoLuong", txtSL.Text);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                gf.fHienthiDulieuDatagridView("vChitietPN where [Số phiếu nhập] ='" + txtSoPN.Text + "'", dgrChitietPN);
            }
            catch
            {
                MessageBox.Show("Có lỗi khi thêm sản phẩm!", "Chú ý");
            }
        }

    }
}
