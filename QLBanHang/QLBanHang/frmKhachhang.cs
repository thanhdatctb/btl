﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace QLBanHang
{
    public partial class frmKhachhang : Form
    {
        GlobalFuncion gf = new GlobalFuncion();
        string sView = "vKhachhang";
        public frmKhachhang()
        {
            InitializeComponent();
        }

        private void frmKhachhang_Load(object sender, EventArgs e)
        {
            gf.fHienthiDulieuDatagridView(sView, dgrKhachhang);
        }

        private void dgrKhachhang_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgrKhachhang.Rows.Count <= 0 || dgrKhachhang.CurrentRow.Index == dgrKhachhang.Rows.Count - 1)
                return;
            txtMaKH.Text = "" + dgrKhachhang.CurrentRow.Cells[0].Value.ToString();
            txtTenKH.Text = "" + dgrKhachhang.CurrentRow.Cells[1].Value.ToString();
            txtDiachi.Text = "" + dgrKhachhang.CurrentRow.Cells[2].Value.ToString();
            txtSDT.Text = "" + dgrKhachhang.CurrentRow.Cells[3].Value.ToString();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if (gf.fKiemtraXaurong(txtMaKH.Text) == true || gf.fKiemtraXaurong(txtTenKH.Text) == true || gf.fKiemtraXaurong(txtDiachi.Text) == true || gf.fKiemtraXaurong(txtSDT.Text) == true)
            {
                MessageBox.Show("Chưa nhập đủ thông tin khách hàng", "Chú ý");
                return;
            }
            if (gf.fKiemtraTrungkhoa("tblKhachHang", "MaKH", txtMaKH.Text) == true)
            {
                MessageBox.Show("Mã khách hàng này đã có!", "Chú ý");
                return;
            }
            try
            {
                if (gf.fKetNoiCSDL() == false)
                    return;
                SqlCommand cmd = new SqlCommand("Ins_KhachHang", gf.myCnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MaKH", txtMaKH.Text);
                cmd.Parameters.AddWithValue("@TenKH", txtTenKH.Text);
                cmd.Parameters.AddWithValue("@DiaChi", txtDiachi.Text);
                cmd.Parameters.AddWithValue("@SDT", txtSDT.Text);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                gf.fHienthiDulieuDatagridView(sView, dgrKhachhang);
            }
            catch
            {
                MessageBox.Show("Có lỗi khi thêm dữ liệu!", "Chú ý");
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (gf.fKiemtraXaurong(txtMaKH.Text) == true || gf.fKiemtraXaurong(txtTenKH.Text) == true || gf.fKiemtraXaurong(txtDiachi.Text) == true || gf.fKiemtraXaurong(txtSDT.Text) == true)
            {
                MessageBox.Show("Chưa nhập đủ thông tin khách hàng", "Chú ý");
                return;
            }
            try
            {
                int nVitri = dgrKhachhang.CurrentRow.Index;
                if (gf.fKetNoiCSDL() == false)
                    return;
                SqlCommand cmd = new SqlCommand("spSuaKH", gf.myCnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MaKH", txtMaKH.Text);
                cmd.Parameters.AddWithValue("@TenKH", txtTenKH.Text);
                cmd.Parameters.AddWithValue("@Diachi", txtDiachi.Text);
                cmd.Parameters.AddWithValue("@SDT", txtSDT.Text);
                //cmd.Parameters.AddWithValue("@LuuKho", txtDiachi.Text);
                cmd.Parameters.AddWithValue("@MaCansua", dgrKhachhang.CurrentRow.Cells[0].Value.ToString());
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                gf.fHienthiDulieuDatagridView(sView, dgrKhachhang);
                //Chọn dòng vừa sửa
                dgrKhachhang.ClearSelection();
                dgrKhachhang.CurrentCell = dgrKhachhang.Rows[nVitri].Cells[0];
                dgrKhachhang.Rows[nVitri].Selected = true;
            }
            catch
            {
                MessageBox.Show("Có lỗi khi sửa dữ liệu!", "Chú ý");
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (dgrKhachhang.RowCount <= 0)
                return;
            if (MessageBox.Show("Bạn có muốn xóa khách hàng này?", "Chú ý", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;
            try
            {
                if (!gf.fKetNoiCSDL())
                    return;
                using (SqlCommand cmd = new SqlCommand("spXoaKH", gf.myCnn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@MaCanxoa", dgrKhachhang.CurrentRow.Cells[0].Value.ToString());
                    cmd.ExecuteNonQuery();
                    gf.fHienthiDulieuDatagridView(sView, dgrKhachhang);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi: " + ex.Message, "Thông báo");
            }
        }
    }
}
