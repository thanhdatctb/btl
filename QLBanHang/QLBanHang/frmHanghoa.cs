﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace QLBanHang
{
    public partial class frmHanghoa : Form
    {
        GlobalFuncion gf = new GlobalFuncion();
        string sView = "vHanghoa";
        public frmHanghoa()
        {
            InitializeComponent();
        }

        private void frmHanghoa_Load(object sender, EventArgs e)
        {
             gf.fHienthiDulieuDatagridView(sView, dgrHanghoa);
        }

        private void dgrHanghoa_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgrHanghoa.Rows.Count <= 0 || dgrHanghoa.CurrentRow.Index == dgrHanghoa.Rows.Count - 1)
                return;
            txtMaHH.Text = "" + dgrHanghoa.CurrentRow.Cells[0].Value.ToString();
            txtTenHH.Text = "" + dgrHanghoa.CurrentRow.Cells[1].Value.ToString();
            txtDVT.Text = "" + dgrHanghoa.CurrentRow.Cells[2].Value.ToString();
            txtGiaban.Text = "" + dgrHanghoa.CurrentRow.Cells[3].Value.ToString();
            txtMaLH.Text = "" + dgrHanghoa.CurrentRow.Cells[4].Value.ToString();

        }

        private void btnThem_Click(object sender, EventArgs e)
        {

            if (gf.fKiemtraXaurong(txtMaHH.Text) == true || gf.fKiemtraXaurong(txtTenHH.Text) == true || gf.fKiemtraXaurong(txtDVT.Text) == true || gf.fKiemtraXaurong(txtMaLH.Text) == true || gf.fKiemtraXaurong(txtGiaban.Text) == true)
            {
                MessageBox.Show("Chưa nhập đủ thông tin hàng hóa", "Chú ý");
                return;
            }
            if (gf.fKiemtraTrungkhoa("tblHangHoa", "MaH", txtMaHH.Text) == true)
            {
                MessageBox.Show("Mã hàng hóa này đã có!", "Chú ý");
                return;
            }
            try
            {
                if (gf.fKetNoiCSDL() == false)
                    return;
                SqlCommand cmd = new SqlCommand("Ins_HangHoa", gf.myCnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MaH", txtMaHH.Text);
                cmd.Parameters.AddWithValue("@TenH", txtTenHH.Text);
                cmd.Parameters.AddWithValue("@DVTinh", txtDVT.Text);
                cmd.Parameters.AddWithValue("@Giaban", txtGiaban.Text);
                cmd.Parameters.AddWithValue("@Luukho", txtLuuKho.Text);
                cmd.Parameters.AddWithValue("@MaL", txtMaLH.Text);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                gf.fHienthiDulieuDatagridView(sView, dgrHanghoa);
            }
            catch
            {
                MessageBox.Show("Có lỗi khi thêm dữ liệu!", "Chú ý");
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (gf.fKiemtraXaurong(txtMaHH.Text) == true || gf.fKiemtraXaurong(txtTenHH.Text) == true || gf.fKiemtraXaurong(txtDVT.Text) == true || gf.fKiemtraXaurong(txtMaLH.Text) == true || gf.fKiemtraXaurong(txtGiaban.Text) == true)
            {
                MessageBox.Show("Chưa nhập đủ thông tin hàng hóa", "Chú ý");
                return;
            }
            if (gf.fKiemtraTrungkhoa("tblHangHoa", "MaH", txtMaHH.Text) == false)
            {
                MessageBox.Show("Mã hàng này đã có!", "Chú ý");
                return;
            }
          //  try
          //  {
                int nVitri = dgrHanghoa.CurrentRow.Index;
                if (gf.fKetNoiCSDL() == false)
                    return;
                SqlCommand cmd = new SqlCommand("spSuaHH", gf.myCnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MaH", txtMaHH.Text);
                cmd.Parameters.AddWithValue("@TenH", txtTenHH.Text);
                cmd.Parameters.AddWithValue("@DVT", txtDVT.Text);
                cmd.Parameters.AddWithValue("@Giaban", txtGiaban.Text);
                cmd.Parameters.AddWithValue("@MaL", txtMaLH.Text);
                cmd.Parameters.AddWithValue("@MaL", txtMaLH.Text);
                cmd.Parameters.AddWithValue("@Macansua", dgrHanghoa.CurrentRow.Cells[0].Value.ToString());
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                gf.fHienthiDulieuDatagridView(sView, dgrHanghoa);
                //Chọn dòng vừa sửa
                dgrHanghoa.ClearSelection();
                dgrHanghoa.CurrentCell = dgrHanghoa.Rows[nVitri].Cells[0];
                dgrHanghoa.Rows[nVitri].Selected = true;
          //  }
          //  catch (Exception ex)
          //  {
          //      MessageBox.Show("Lỗi: " + ex.Message, "Thông báo");
         //   }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (dgrHanghoa.RowCount <= 0)
                return;
            if (MessageBox.Show("Bạn có muốn xóa hàng hóa này?", "Chú ý", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;
            try
            {
                if (!gf.fKetNoiCSDL())
                    return;
                using (SqlCommand cmd = new SqlCommand("spXoaHH", gf.myCnn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@MaCanxoa", dgrHanghoa.CurrentRow.Cells[0].Value.ToString());
                    cmd.ExecuteNonQuery();
                    gf.fHienthiDulieuDatagridView(sView, dgrHanghoa);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi: " + ex.Message, "Thông báo");
            }
        }
    }
}
