﻿namespace QLBanHang
{
    partial class frmTimkiemNV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnIn = new System.Windows.Forms.Button();
            this.btnTim = new System.Windows.Forms.Button();
            this.txtDieukien = new System.Windows.Forms.TextBox();
            this.rbtTimtheoTen = new System.Windows.Forms.RadioButton();
            this.rbtTimDC = new System.Windows.Forms.RadioButton();
            this.rbtTimtheoMaNV = new System.Windows.Forms.RadioButton();
            this.rbtTatca = new System.Windows.Forms.RadioButton();
            this.dgrTimNV = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrTimNV)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnIn);
            this.groupBox1.Controls.Add(this.btnTim);
            this.groupBox1.Controls.Add(this.txtDieukien);
            this.groupBox1.Controls.Add(this.rbtTimtheoTen);
            this.groupBox1.Controls.Add(this.rbtTimDC);
            this.groupBox1.Controls.Add(this.rbtTimtheoMaNV);
            this.groupBox1.Controls.Add(this.rbtTatca);
            this.groupBox1.Location = new System.Drawing.Point(31, 219);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(713, 220);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Chọn tiêu chí";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // btnIn
            // 
            this.btnIn.Location = new System.Drawing.Point(471, 107);
            this.btnIn.Margin = new System.Windows.Forms.Padding(4);
            this.btnIn.Name = "btnIn";
            this.btnIn.Size = new System.Drawing.Size(203, 28);
            this.btnIn.TabIndex = 3;
            this.btnIn.Text = "Xem Danh sách nhân viên";
            this.btnIn.UseVisualStyleBackColor = true;
            this.btnIn.Click += new System.EventHandler(this.btnIn_Click);
            // 
            // btnTim
            // 
            this.btnTim.Location = new System.Drawing.Point(471, 44);
            this.btnTim.Margin = new System.Windows.Forms.Padding(4);
            this.btnTim.Name = "btnTim";
            this.btnTim.Size = new System.Drawing.Size(203, 28);
            this.btnTim.TabIndex = 3;
            this.btnTim.Text = "Tìm";
            this.btnTim.UseVisualStyleBackColor = true;
            this.btnTim.Click += new System.EventHandler(this.btnTim_Click);
            // 
            // txtDieukien
            // 
            this.txtDieukien.Location = new System.Drawing.Point(201, 110);
            this.txtDieukien.Margin = new System.Windows.Forms.Padding(4);
            this.txtDieukien.Name = "txtDieukien";
            this.txtDieukien.Size = new System.Drawing.Size(169, 22);
            this.txtDieukien.TabIndex = 1;
            // 
            // rbtTimtheoTen
            // 
            this.rbtTimtheoTen.AutoSize = true;
            this.rbtTimtheoTen.Location = new System.Drawing.Point(4, 110);
            this.rbtTimtheoTen.Margin = new System.Windows.Forms.Padding(4);
            this.rbtTimtheoTen.Name = "rbtTimtheoTen";
            this.rbtTimtheoTen.Size = new System.Drawing.Size(108, 21);
            this.rbtTimtheoTen.TabIndex = 0;
            this.rbtTimtheoTen.Text = "Tìm theo tên";
            this.rbtTimtheoTen.UseVisualStyleBackColor = true;
            this.rbtTimtheoTen.CheckedChanged += new System.EventHandler(this.rbtTimtheoTen_CheckedChanged);
            // 
            // rbtTimDC
            // 
            this.rbtTimDC.AutoSize = true;
            this.rbtTimDC.Location = new System.Drawing.Point(4, 155);
            this.rbtTimDC.Margin = new System.Windows.Forms.Padding(4);
            this.rbtTimDC.Name = "rbtTimDC";
            this.rbtTimDC.Size = new System.Drawing.Size(128, 21);
            this.rbtTimDC.TabIndex = 0;
            this.rbtTimDC.Text = "Tìm theo địc chỉ";
            this.rbtTimDC.UseVisualStyleBackColor = true;
            this.rbtTimDC.CheckedChanged += new System.EventHandler(this.rbtTimMaDC_CheckedChanged);
            // 
            // rbtTimtheoMaNV
            // 
            this.rbtTimtheoMaNV.AutoSize = true;
            this.rbtTimtheoMaNV.Location = new System.Drawing.Point(4, 63);
            this.rbtTimtheoMaNV.Margin = new System.Windows.Forms.Padding(4);
            this.rbtTimtheoMaNV.Name = "rbtTimtheoMaNV";
            this.rbtTimtheoMaNV.Size = new System.Drawing.Size(173, 21);
            this.rbtTimtheoMaNV.TabIndex = 0;
            this.rbtTimtheoMaNV.Text = "Tìm theo Mã nhân viên";
            this.rbtTimtheoMaNV.UseVisualStyleBackColor = true;
            this.rbtTimtheoMaNV.CheckedChanged += new System.EventHandler(this.rbtTimtheoMaNV_CheckedChanged);
            // 
            // rbtTatca
            // 
            this.rbtTatca.AutoSize = true;
            this.rbtTatca.Checked = true;
            this.rbtTatca.Location = new System.Drawing.Point(4, 20);
            this.rbtTatca.Margin = new System.Windows.Forms.Padding(4);
            this.rbtTatca.Name = "rbtTatca";
            this.rbtTatca.Size = new System.Drawing.Size(97, 21);
            this.rbtTatca.TabIndex = 0;
            this.rbtTatca.TabStop = true;
            this.rbtTatca.Text = "Hiện tất cả";
            this.rbtTatca.UseVisualStyleBackColor = true;
            this.rbtTatca.CheckedChanged += new System.EventHandler(this.rbtTatca_CheckedChanged);
            // 
            // dgrTimNV
            // 
            this.dgrTimNV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrTimNV.Location = new System.Drawing.Point(31, 15);
            this.dgrTimNV.Margin = new System.Windows.Forms.Padding(4);
            this.dgrTimNV.Name = "dgrTimNV";
            this.dgrTimNV.RowHeadersWidth = 51;
            this.dgrTimNV.Size = new System.Drawing.Size(729, 185);
            this.dgrTimNV.TabIndex = 2;
            // 
            // frmTimkiemNV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 447);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgrTimNV);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmTimkiemNV";
            this.Text = "frmTimkiemNV";
            this.Load += new System.EventHandler(this.frmTimkiemNV_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrTimNV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnIn;
        private System.Windows.Forms.Button btnTim;
        private System.Windows.Forms.TextBox txtDieukien;
        private System.Windows.Forms.RadioButton rbtTimtheoTen;
        private System.Windows.Forms.RadioButton rbtTimDC;
        private System.Windows.Forms.RadioButton rbtTimtheoMaNV;
        private System.Windows.Forms.RadioButton rbtTatca;
        private System.Windows.Forms.DataGridView dgrTimNV;
    }
}