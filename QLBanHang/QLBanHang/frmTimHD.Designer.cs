﻿namespace QLBanHang
{
    partial class FrmTimHD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgrTimHD = new System.Windows.Forms.DataGridView();
            this.grbTimHD = new System.Windows.Forms.GroupBox();
            this.btnTim = new System.Windows.Forms.Button();
            this.txtDieukien = new System.Windows.Forms.TextBox();
            this.rbtTimtheoTenKH = new System.Windows.Forms.RadioButton();
            this.rbtTimtheongaylap = new System.Windows.Forms.RadioButton();
            this.rbtTimtheoMaHD = new System.Windows.Forms.RadioButton();
            this.rbtTatca = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgrTimHD)).BeginInit();
            this.grbTimHD.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgrTimHD
            // 
            this.dgrTimHD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrTimHD.Location = new System.Drawing.Point(2, -2);
            this.dgrTimHD.Name = "dgrTimHD";
            this.dgrTimHD.Size = new System.Drawing.Size(825, 201);
            this.dgrTimHD.TabIndex = 0;
            // 
            // grbTimHD
            // 
            this.grbTimHD.Controls.Add(this.btnTim);
            this.grbTimHD.Controls.Add(this.txtDieukien);
            this.grbTimHD.Controls.Add(this.rbtTimtheoTenKH);
            this.grbTimHD.Controls.Add(this.rbtTimtheongaylap);
            this.grbTimHD.Controls.Add(this.rbtTimtheoMaHD);
            this.grbTimHD.Controls.Add(this.rbtTatca);
            this.grbTimHD.Location = new System.Drawing.Point(2, 219);
            this.grbTimHD.Name = "grbTimHD";
            this.grbTimHD.Size = new System.Drawing.Size(796, 180);
            this.grbTimHD.TabIndex = 1;
            this.grbTimHD.TabStop = false;
            this.grbTimHD.Text = "Chọn tiêu chí";
            // 
            // btnTim
            // 
            this.btnTim.Location = new System.Drawing.Point(489, 76);
            this.btnTim.Name = "btnTim";
            this.btnTim.Size = new System.Drawing.Size(89, 35);
            this.btnTim.TabIndex = 5;
            this.btnTim.Text = "Tìm";
            this.btnTim.UseVisualStyleBackColor = true;
            this.btnTim.Click += new System.EventHandler(this.btnTim_Click);
            // 
            // txtDieukien
            // 
            this.txtDieukien.Location = new System.Drawing.Point(205, 101);
            this.txtDieukien.Name = "txtDieukien";
            this.txtDieukien.Size = new System.Drawing.Size(100, 20);
            this.txtDieukien.TabIndex = 4;
            // 
            // rbtTimtheoTenKH
            // 
            this.rbtTimtheoTenKH.AutoSize = true;
            this.rbtTimtheoTenKH.Location = new System.Drawing.Point(32, 144);
            this.rbtTimtheoTenKH.Name = "rbtTimtheoTenKH";
            this.rbtTimtheoTenKH.Size = new System.Drawing.Size(144, 17);
            this.rbtTimtheoTenKH.TabIndex = 3;
            this.rbtTimtheoTenKH.Text = "Tìm theo tên khách hàng";
            this.rbtTimtheoTenKH.UseVisualStyleBackColor = true;
            this.rbtTimtheoTenKH.CheckedChanged += new System.EventHandler(this.rbtTimtheoTenKH_CheckedChanged);
            // 
            // rbtTimtheongaylap
            // 
            this.rbtTimtheongaylap.AutoSize = true;
            this.rbtTimtheongaylap.Location = new System.Drawing.Point(32, 104);
            this.rbtTimtheongaylap.Name = "rbtTimtheongaylap";
            this.rbtTimtheongaylap.Size = new System.Drawing.Size(109, 17);
            this.rbtTimtheongaylap.TabIndex = 2;
            this.rbtTimtheongaylap.Text = "Tìm theo ngày lập";
            this.rbtTimtheongaylap.UseVisualStyleBackColor = true;
            this.rbtTimtheongaylap.CheckedChanged += new System.EventHandler(this.rbtTimtheongaylap_CheckedChanged);
            // 
            // rbtTimtheoMaHD
            // 
            this.rbtTimtheoMaHD.AutoSize = true;
            this.rbtTimtheoMaHD.Location = new System.Drawing.Point(32, 62);
            this.rbtTimtheoMaHD.Name = "rbtTimtheoMaHD";
            this.rbtTimtheoMaHD.Size = new System.Drawing.Size(126, 17);
            this.rbtTimtheoMaHD.TabIndex = 1;
            this.rbtTimtheoMaHD.Text = "Tìm theo mã hóa đơn";
            this.rbtTimtheoMaHD.UseVisualStyleBackColor = true;
            this.rbtTimtheoMaHD.CheckedChanged += new System.EventHandler(this.rbtTimtheoMaHD_CheckedChanged);
            // 
            // rbtTatca
            // 
            this.rbtTatca.AutoSize = true;
            this.rbtTatca.Checked = true;
            this.rbtTatca.Location = new System.Drawing.Point(32, 20);
            this.rbtTatca.Name = "rbtTatca";
            this.rbtTatca.Size = new System.Drawing.Size(77, 17);
            this.rbtTatca.TabIndex = 0;
            this.rbtTatca.TabStop = true;
            this.rbtTatca.Text = "Hiện tất cả";
            this.rbtTatca.UseVisualStyleBackColor = true;
            this.rbtTatca.CheckedChanged += new System.EventHandler(this.rbtTatca_CheckedChanged);
            // 
            // FrmTimHD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 401);
            this.Controls.Add(this.grbTimHD);
            this.Controls.Add(this.dgrTimHD);
            this.Name = "FrmTimHD";
            this.Text = "frmTimHD";
            this.Load += new System.EventHandler(this.FrmTimHD_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgrTimHD)).EndInit();
            this.grbTimHD.ResumeLayout(false);
            this.grbTimHD.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgrTimHD;
        private System.Windows.Forms.GroupBox grbTimHD;
        private System.Windows.Forms.RadioButton rbtTimtheoMaHD;
        private System.Windows.Forms.RadioButton rbtTatca;
        private System.Windows.Forms.RadioButton rbtTimtheoTenKH;
        private System.Windows.Forms.RadioButton rbtTimtheongaylap;
        private System.Windows.Forms.Button btnTim;
        private System.Windows.Forms.TextBox txtDieukien;
    }
}