﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace QLBanHang
{
    public partial class frmLoaihang : Form
    {
        GlobalFuncion gf = new GlobalFuncion();
        string sView = "vLoaiHang";
        public frmLoaihang()
        {
            InitializeComponent();
        }

        private void frmLoaihang_Load(object sender, EventArgs e)
        {
            gf.fHienthiDulieuDatagridView(sView, dgrLoaihang);
        }

        private void dgrLoaihang_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgrLoaihang.Rows.Count <= 0 || dgrLoaihang.CurrentRow.Index == dgrLoaihang.Rows.Count - 1)
                return;
            txtMaL.Text = "" + dgrLoaihang.CurrentRow.Cells[0].Value.ToString();
            txtTenL.Text = "" + dgrLoaihang.CurrentRow.Cells[1].Value.ToString();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if (gf.fKiemtraXaurong(txtMaL.Text) == true || gf.fKiemtraXaurong(txtTenL.Text) == true)
            {
                MessageBox.Show("Chưa nhập đủ thông tin loại hàng", "Chú ý");
                return;
            }
            if (gf.fKiemtraTrungkhoa("tblLoaiHang", "MaL", txtMaL.Text) == true)
            {
                MessageBox.Show("Mã loại hàng này đã có!", "Chú ý");
                return;
            }
            try
            {
                if (gf.fKetNoiCSDL() == false)
                    return;
                SqlCommand cmd = new SqlCommand("Ins_LoaiHang", gf.myCnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MaL", txtMaL.Text);
                cmd.Parameters.AddWithValue("@TenL", txtTenL.Text);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                gf.fHienthiDulieuDatagridView(sView, dgrLoaihang);
            }
            catch
            {
                MessageBox.Show("Có lỗi khi thêm dữ liệu!", "Chú ý");
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (gf.fKiemtraXaurong(txtMaL.Text) == true || gf.fKiemtraXaurong(txtTenL.Text) == true)
            {
                MessageBox.Show("Chưa nhập đủ thông tin loại hàng", "Chú ý");
                return;
            }
            try
            {
                int nVitri = dgrLoaihang.CurrentRow.Index;
                if (gf.fKetNoiCSDL() == false)
                    return;
                SqlCommand cmd = new SqlCommand("spSuaLH", gf.myCnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MaL", txtMaL.Text);
                cmd.Parameters.AddWithValue("@TenL", txtTenL.Text);
                cmd.Parameters.AddWithValue("@MaCansua", dgrLoaihang.CurrentRow.Cells[0].Value.ToString());
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                gf.fHienthiDulieuDatagridView(sView, dgrLoaihang);
                //Chọn dòng vừa sửa
                dgrLoaihang.ClearSelection();
                dgrLoaihang.CurrentCell = dgrLoaihang.Rows[nVitri].Cells[0];
                dgrLoaihang.Rows[nVitri].Selected = true;
            }
            catch
            {
                MessageBox.Show("Có lỗi khi sửa dữ liệu!", "Chú ý");
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (dgrLoaihang.RowCount <= 0)
                return;
            if (MessageBox.Show("Bạn có muốn xóa loại hàng này?", "Chú ý", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;
            try
            {
                if (!gf.fKetNoiCSDL())
                    return;
                using (SqlCommand cmd = new SqlCommand("spXoaLH", gf.myCnn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@MaCanxoa", dgrLoaihang.CurrentRow.Cells[0].Value.ToString());
                    cmd.ExecuteNonQuery();
                    gf.fHienthiDulieuDatagridView(sView, dgrLoaihang);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi: " + ex.Message, "Thông báo");
            }
        }

    }
    
}
