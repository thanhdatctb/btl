﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace QLBanHang
{
    class GlobalFuncion
    {
        //Khai Báo thuộc tính toàn cục
        string strConnection = @"Data Source=DESKTOP-O0990HT\SQLEXPRESS;Initial Catalog=QLBH1;Integrated Security=True";

        public SqlConnection myCnn = new SqlConnection();
        //Xây dựng các phương thức kết nối
        //1. Kết nối CSDL
        public bool fKetNoiCSDL()
        {
            try
            {
                if (myCnn.State == ConnectionState.Open)
                    myCnn.Close();
                myCnn.ConnectionString = strConnection;
                myCnn.Open();
            }
            catch
            {
                MessageBox.Show("Lỗi kết nối tới CSDL", "Chú ý");
                return false;
            }
            return true;
        }

        public void Ngatketnoi()
        {
            if (myCnn.State != 0)
            {
                myCnn.Close();
            }
        }
        //Phương thức xem DL
        public DataTable XemDL(string sql)
        {
            fKetNoiCSDL();

            SqlDataAdapter adap = new SqlDataAdapter(sql, myCnn);
            DataTable dt = new DataTable();
            adap.Fill(dt);

            return dt;
            Ngatketnoi();
        }
        //2 Hiển thị các thông tin điều khiển 
        //2.1 hiển thị các dữ liệu trên bảng DataGribView
        public void fHienthiDulieuDatagridView(string sSQL, DataGridView dgr)
        {
            try
            {
                if (fKetNoiCSDL() == false)
                    return;
                SqlDataAdapter da = new SqlDataAdapter("Select * from " + sSQL, myCnn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dgr.DataSource = dt;
                dgr.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                dgr.AutoResizeColumns();
            }
            catch
            {
                MessageBox.Show("Lỗi hiển thị dữ liệu", "Chú ý");
            }
        }
        //2.2. Hiển thị dữ liệu trên ComboBox
       public void fHienthiDulieuComboBox(string sTenbang,
            string sCotHienthi, string sCotKhoa, ComboBox cbo)
        {
            try
            {
                if (fKetNoiCSDL() == false)
                    return;
                SqlDataAdapter da = new SqlDataAdapter("Select * from " + sTenbang, myCnn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                cbo.DataSource = dt;
                cbo.DisplayMember = sCotHienthi;
                cbo.ValueMember = sCotKhoa;
            }
            catch
            {
                MessageBox.Show("Lỗi hiển thị dữ liệu", "Chú ý");
            }

        }


        //3. Kiểm tra tính hợp lệ của dữ liệu
        //3.1. Kiểm tra xâu rỗng?
        public bool fKiemtraXaurong(string s)
        {
            if (s.Trim().Length == 0)
                return true;
            return false;
        }

        //3.2. Kiểm tra kiểu số nguyên
        public bool fKiemtraSonguyen(string s)
        {
            try
            {
                int k = int.Parse(s);
            }
            catch
            {
                return false;
            }
            return true;
        }

        //3.3. Kiểm tra kiểu số thực
        public bool fKiemtraSothuc(string s)
        {
            try
            {
                float k = float.Parse(s);
            }
            catch
            {
                return false;
            }
            return true;
        }

        //3.4. Kiểm tra kiểu ngày tháng
        public bool fKiemtraNgaythang(string s)
        {
            try
            {
                DateTime d = DateTime.Parse(s);
            }
            catch
            {
                return false;
            }
            return true;
        }

        //3.5. ...

        //4. Kiểm tra sự tồn tại của khóa chính
        public bool fKiemtraTrungkhoa(string sTenbang, string sTencotKhoa, string sGiatri)
        {
            try
            {
                if (fKetNoiCSDL() == false)
                    return true;
                string s = "Select * from " + sTenbang
                    + " where " + sTencotKhoa + "='" + sGiatri + "'";
                SqlDataAdapter da = new SqlDataAdapter(s, myCnn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                    return true;
            }
            catch
            {
                return true;
            }
            return false;
        }

        //5. Hàm xóa dữ liệu
        public void fXoaDulieu(string sTenThutuc, string sGiatri)
        {
            try
            {
                if (fKetNoiCSDL() == false)
                    return;
                SqlCommand cmd = new SqlCommand(sTenThutuc, myCnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MaCanxoa", sGiatri);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch
            {
                MessageBox.Show("Lỗi xóa dữ liệu!", "Chú ý");
            }

            //6 kiem tra tuo from " + Tenbang

        }
        
    }
}
