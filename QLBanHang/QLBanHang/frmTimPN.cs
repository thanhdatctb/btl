﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QLBanHang
{
    public partial class frmTimPN : Form
    {

        GlobalFuncion gf = new GlobalFuncion();
        string sView = "vPhieunhap";
        string sTimkiem = "";
        public frmTimPN()
        {
            InitializeComponent();
        }

        
        private void frmTimPN_Load(object sender, EventArgs e)
        {
            gf.fHienthiDulieuDatagridView(sView, dgrTimPN);
            txtDieukien.Visible = false;
            sTimkiem = sView;

        }
        private void rbtTatca_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = false;
        }

        private void rbtTimtheoSoPN_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = true;
            txtDieukien.Top = rbtTimtheoSoPN.Top;
        }

        private void rbtTimtheoNgaynhap_CheckedChanged(object sender, EventArgs e)
        {
            txtDieukien.Visible = true;
            txtDieukien.Top = rbtTimtheoNgaynhap.Top;
            
        }

        private void btnTim_Click(object sender, EventArgs e)
        {
            string strSQL = sView;
            if (rbtTimtheoSoPN.Checked)
                strSQL = strSQL + " WHERE [Số phiếu nhập]='" + txtDieukien.Text + "'";
            if (rbtTimtheoNgaynhap.Checked)
            {
                strSQL = strSQL + " WHERE [Ngày nhập] = '" + txtDieukien.Text + "'";
                if (gf.fKiemtraNgaythang(txtDieukien.Text) == false)
                {
                    MessageBox.Show("Sai ngay tháng!", "Chú ý");
                    return;
                }
            }
            gf.fHienthiDulieuDatagridView(strSQL, dgrTimPN);
            sTimkiem = strSQL;

        }

    }
}
